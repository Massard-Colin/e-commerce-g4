/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
require('@fortawesome/fontawesome-free/css/all.min.css');
require('@fortawesome/fontawesome-free/js/all.js');
import '../stylesheets/application.sass';
import '../css/app.css';
const router = require('./router.js');
import Routing from './router';


import $ from 'jquery'
import Slider from './components/slider';
import InputNumber from './components/input-number';
import Header from './components/header';
import Modal from './components/modal';
import Commande from './components/commande';
import MenuAdmin from './components/menu-admin';

document.addEventListener('DOMContentLoaded', function(){
  new Slider()
  new InputNumber()
  new Header()
  new Modal()
  new Commande()
  new MenuAdmin()	
})

