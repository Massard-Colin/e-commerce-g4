export default function() {

  let menu = document.getElementById("header-responsive")

if (menu != undefined) {
   menu.addEventListener('click', function() {

    this.classList.toggle("change");
    menu = document.getElementsByClassName("header")[0].classList.toggle("responsive");
    menu = document.getElementsByClassName("header__navbar-button--responsive-navbar");
    for (var i = 0; i < menu.length; i++) {
      menu[i].classList.toggle("hidden");
      menu[i].classList.toggle("block");
      menu[i].classList.toggle("float-none");
      menu[i].classList.toggle("text-center");
    }
  });
}


}
