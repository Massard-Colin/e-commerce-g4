import $ from 'jquery'
const router = require('../router.js');
export default () => {

  // INPUT NUMBER
  $('<button class="input--number__btn input--number__btn--down"></button>').insertBefore('.input--number__field')
  $('<button class="input--number__btn input--number__btn--up"></button>').insertAfter('.input--number__field')

  $('.input--number').each(function() {
    var spinner = $(this),
    input = spinner.find('.input--number__field'),
    btnUp = spinner.find('.input--number__btn.input--number__btn--up'),
    btnDown = spinner.find('.input--number__btn.input--number__btn--down'),
    min = input.attr('min'),
    max = input.attr('max'),
    step = input.attr('step')
    btnUp.click(function() {
      var oldValue = parseFloat(input.val())
      if (oldValue >= max) {
        var newVal = oldValue
      } else {
        var newVal = oldValue + parseFloat(step)
        var id = $(input).data('id');
        var articleId = $(input).data('article');
        console.log(id);
        $.ajax({
          type: 'get',
          data: { article : articleId},
          url: '/account/shopping/article/addquantity/'+id,
          dataType: 'json',
          success: function(data) {
            console.log('plus')

          },
        })
      }
      spinner.find("input").val(newVal)
      spinner.find("input").trigger("change")
    })

    btnDown.click(function() {
      var oldValue = parseFloat(input.val())
      if (oldValue === 1) {
       var id = $(input).data('id');
       var articleId = $(input).data('article');
       if ( confirm( "Supprimer l'article du panier ? " ) ) {
        $(input).parent().parent().parent().remove()
        
        $.ajax({
          type: 'get',
          data: { article : articleId},
          url: '/account/shopping/remove'+id,
          dataType: 'json',
          success: function(data) {

          },
        })
      }
    }
    else {
      if (oldValue <= min) {
        var newVal = oldValue
      } else {
        var id = $(input).data('id');
        var articleId = $(input).data('article');
        $.ajax({
          type: 'get',
          data: { article : articleId},
          url: '/account/shopping/article/rmquantity/'+id,
          dataType: 'json',
          success: function(data) {

          },
        })
        var newVal = oldValue - parseFloat(step)
        console.log(newVal+'/'+id)

      }
      spinner.find("input").val(newVal)
      spinner.find("input").trigger("change")
    }
  })
    $('#addShopping').click(function() {
      var id = $(this).data('id');
      console.log(id);
      $.ajax({
        type: 'POST',
        url: router.routing.generate('account_add_shopping'),
        dataType: 'json',
        success: function(data) {

        },
      })
    })
  })
}
