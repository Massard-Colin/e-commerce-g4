import $ from 'jquery'
export default function() {


    // SLIDER
    var prevButton = document.getElementById('slideshow-prev');
    var nextButton = document.getElementById('slideshow-next');
    if (prevButton != undefined && nextButton != undefined) {
      var slideWidth = document.querySelector('.slideshow--content--item').offsetWidth;
      var slide = document.querySelector('.slideshow--content--item');
      var indexSlider = 0;

      buttonDisplay(indexSlider)

      window.addEventListener('resize',function(){
        slideWidth = document.querySelector('.slideshow--content--item').offsetWidth;
        var slide = document.querySelector('.slideshow--content--item');

        $(slide).animate({
          marginLeft: "0px"
        },500);

        indexSlider = 0;
        buttonDisplay(indexSlider);
      });

      prevButton.addEventListener('click',function(){

        $(slide).animate({
          marginLeft: "+="+slideWidth
        },1000);
        indexSlider--
        buttonDisplay(indexSlider);
      });
      nextButton.addEventListener('click',function(){
        $(slide).animate({
          marginLeft: "-="+slideWidth
        },1000);
        indexSlider++
        buttonDisplay(indexSlider);
      });
    }

function buttonDisplay(indexSlider){
  var slideNumber = document.querySelectorAll('.slideshow--content--item').length;
  var prevButton = document.getElementById('slideshow-prev');
  var nextButton = document.getElementById('slideshow-next');
  if (indexSlider === 0 || indexSlider === slideNumber-1) {
    if (indexSlider === 0) {
      prevButton.style.display = "none";
      prevButton.style.opacity = 0;
      nextButton.style.display = "block";
      $(nextButton).animate({
        opacity: "1"
      },250);
    }
    else{
      nextButton.style.display = "none";
      nextButton.style.opacity = 0;
      prevButton.style.display = "block";
      $(prevButton).animate({
        opacity: "1"
      },250);
    }
  }

  else {
    prevButton.style.display = "block";
    $(prevButton).animate({
      opacity: "1"
    },250);
    nextButton.style.display = "block";
    $(nextButton).animate({
      opacity: "1"
    },250);
  }
}
}
