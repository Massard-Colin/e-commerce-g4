window.addEventListener("DOMContentLoaded", (event) => {

  console.log("Allons toucher les étoiles ✨💫🚀");

  // INPUT NUMBER
  jQuery('<button class="input--number__btn input--number__btn--down"></button>').insertBefore('.input--number__field');
  jQuery('<button class="input--number__btn input--number__btn--up"></button>').insertAfter('.input--number__field');

  jQuery('.input--number').each(function() {
    var spinner = jQuery(this),
    input = spinner.find('.input--number__field'),
    btnUp = spinner.find('.input--number__btn.input--number__btn--up'),
    btnDown = spinner.find('.input--number__btn.input--number__btn--down'),
    min = input.attr('min'),
    max = input.attr('max'),
    step = input.attr('step');
    btnUp.click(function() {
      var oldValue = parseFloat(input.val());
      if (oldValue >= max) {
        var newVal = oldValue;
      } else {
        var newVal = oldValue + parseFloat(step);
      }
      spinner.find("input").val(newVal);
      spinner.find("input").trigger("change");
    });

    btnDown.click(function() {
      var oldValue = parseFloat(input.val());
      if (oldValue === 1) {
        if ( confirm( "Supprimer l'article du panier ? " ) ) {
            $(input).parent().parent().parent().remove()
          }
      }
      else {
        if (oldValue <= min) {
          var newVal = oldValue;
        } else {
          var newVal = oldValue - parseFloat(step);
        }
        spinner.find("input").val(newVal);
      spinner.find("input").trigger("change");
      }
    });
  });

  jQuery('.cart-item__cancel').each(function() {
    var spinner = jQuery(this);
    spinner.click(function(){
      if (confirm( "Supprimer l'article du panier ? " ) ) {
        spinner.parent().remove();
      }
    })
  });




  // SLIDER
  var prevButton = document.getElementById('slideshow-prev');
  var nextButton = document.getElementById('slideshow-next');
  var slideWidth = document.querySelector('.slideshow--content--item').offsetWidth;
  var slide = document.querySelector('.slideshow--content--item');
  var indexSlider = 0;

  buttonDisplay(indexSlider)

  window.addEventListener('resize',function(){
    slideWidth = document.querySelector('.slideshow--content--item').offsetWidth;
    var slide = document.querySelector('.slideshow--content--item');

    $(slide).animate({
      marginLeft: "0px"
    },500);

    indexSlider = 0;
    buttonDisplay(indexSlider);
  });

  prevButton.addEventListener('click',function(){
    $(slide).animate({
      marginLeft: "+="+slideWidth
    },1000);
    indexSlider--
    buttonDisplay(indexSlider);
  });
  nextButton.addEventListener('click',function(){
    $(slide).animate({
      marginLeft: "-="+slideWidth
    },1000);
    indexSlider++
    buttonDisplay(indexSlider);
  });
})

function buttonDisplay(indexSlider){
  var slideNumber = document.querySelectorAll('.slideshow--content--item').length;
  var prevButton = document.getElementById('slideshow-prev');
  var nextButton = document.getElementById('slideshow-next');


  if (indexSlider === 0 || indexSlider === slideNumber-1) {
    if (indexSlider === 0) {
      prevButton.style.display = "none";
      prevButton.style.opacity = 0;
      nextButton.style.display = "block";
      $(nextButton).animate({
        opacity: "1"
      },250);
    }
    else{
      nextButton.style.display = "none";
      nextButton.style.opacity = 0;
      prevButton.style.display = "block";
      $(prevButton).animate({
        opacity: "1"
      },250);
    }
  }

  else {
    prevButton.style.display = "block";
    $(prevButton).animate({
      opacity: "1"
    },250);
    nextButton.style.display = "block";
    $(nextButton).animate({
      opacity: "1"
    },250);
  }
}
