import $ from 'jquery'
export default function() {

    var btnInscription = document.getElementById('btn-inscrpt')
    var btnConnexion = document.getElementById('btn-connxn')
    if(btnInscription != undefined && btnConnexion != undefined){
        btnInscription.addEventListener('click',function(event){
            event.preventDefault();
            if (event.target.classList.contains("modal")) {
                event.target.style.display = "none";
            }
        });

        btnConnexion.addEventListener('click',function(event){
            event.preventDefault();
            if (event.target.classList.contains("modal")) {
                event.target.style.display = "none";
            }
        });
    }


    $(function () {
        $("*[modal]").click(function (e) {
            $("#" + $(e.target).attr("modal")).css("display", "block");
        });
    });
}
