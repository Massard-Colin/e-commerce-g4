window.onclick = function(event) {
	if (event.target.classList.contains("modal")) {
	  event.target.style.display = "none";
	}
}

$(function(){
	$("*[modal]").click(function(e) {
		$("#" + $(e.target).attr("modal")).css("display","block");
	});
});
