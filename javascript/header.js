function toggleResponsiveHeader(x) {
  x.classList.toggle("change");
  x = document.getElementsByClassName("header")[0].classList.toggle("responsive");
  x = document.getElementsByClassName("header__navbar-button--responsive-navbar");
  for (var i = 0; i < x.length; i++) {
    x[i].classList.toggle("hidden");
    x[i].classList.toggle("block");
    x[i].classList.toggle("float-none");
    x[i].classList.toggle("text-center");
  }
}