<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use App\Entity\Article;
use App\Repository\ArticleRepository;
use App\Entity\ShoppingCartLine;
use App\Repository\ShoppingCartLineRepository;
use App\Form\AccountType;
use App\Form\PasswordUpdateType;
use App\Entity\PasswordUpdate;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormError;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AccountController extends AbstractController
{


    private $em;
    /**
     * @var ShoppingCartLineRepository
     */
    private $shoppingCartLineRepository;

    public function __construct(EntityManagerInterface $em, ArticleRepository $article, ShoppingCartLineRepository $shoppingCartLineRepository)
    {
        $this->em = $em;
        $this->article = $article;
        $this->shoppingCartLineRepository = $shoppingCartLineRepository;
    }
    /**
     * @Route("/account", name="account")
     */
    public function index()
    {
        return $this->render('account/index.html.twig', [
            'controller_name' => 'AccountController',
        ]);
    }

     /**
     * Permet d'afficher et de traiter le formulaire de modification de profil
     * @Route("/account/profile", name="account_profile")
     * @IsGranted("ROLE_USER")
     * 
     */
    public function profile(Request $request)
    {
        $user = $this->getUser();

        $form = $this->createForm(AccountType::class, $user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $this->em->persist($user);
            $this->em->flush();
            $this->addFlash(
                'success',
                "Les données du profil ont été enregistrée avec succès !"
            );

        }
        return $this->render('account/profile.html.twig',[
            'form' => $form->createView()
        ]);
    }

     /**
     * @Route("/account/shopping", name="account_shopping")
     * @IsGranted("ROLE_USER")
     * 
     */
    public function shopping(Request $request)
    {
        $shops = $this->getUser()->getShoppingCartLines();
        $total = $this->shoppingCartLineRepository->findPrice($this->getUser());
        $total = $total ? (float) $total[0]['total'] : (float) 0 ;
        return $this->render('account/shopping.html.twig',[
           'shops' => $shops,
           'total' => $total,
        ]);
    }


     /**
     * @Route("/account/shopping/article/addquantity/{id}", name="account_add_shopping", options = { "expose" = true })
     *
     */
    public function addQuantityArticleShopping(Request $request, ShoppingCartLine $shop)
    {
        $add = $shop->getQuantity();
        $shop->setQuantity($add +1);
        $this->em->persist($shop);
        $this->em->flush();

        return new JsonResponse();
    }


     /**
     * @Route("/account/shopping/article/rmquantity/{id}", name="account_rm_qty_shopping", options = { "expose" = true })
     */
    public function removeQuantityArticleShopping(Request $request, ShoppingCartLine $shop)
    {
        $add = $shop->getQuantity();
        $shop->setQuantity($add -1);
        $this->em->persist($shop);
        $this->em->flush();

        return new JsonResponse();
    }

     /**
     * @Route("/account/shopping/{id}/addarticle", name="account_add_article_shopping")
     * @IsGranted("ROLE_USER")
     */
    public function addArticleShopping(Request $request, Article $article)
    {

        $shop = $this->shoppingCartLineRepository->findArticle($article);
        $shops = $this->getUser()->getShoppingCartLines();

        if($shop) {
             $add = $shop->getQuantity();
             $shop->setQuantity($add +1);
        } else {
            $shop = new ShoppingCartLine;
            $shop->setQuantity(1);
            $shop->setArticle($article);
            $shop->setUser($this->getUser());
        }
        
        $this->em->persist($shop);
        $this->em->flush();

        return $this->redirectToRoute('account_shopping');
    } 

     /**
     * @Route("/account/shopping/remove{id}", name="account_add_hopping", options = { "expose" = true })
     * @IsGranted("ROLE_USER")
     * 
     */
    public function removeArticleShopping(Request $request, ShoppingCartLine $shop)
    {
        //Il faut que je récupère son id 
        $this->em->remove($shop);
        $this->em->flush();

        return new JsonResponse();
    }

       /**
     * Permet de modifier le mot de passe
     *
     * @Route("/account/password", name="account_password")
     * @IsGranted("ROLE_USER")
     * @return Response
     */
    public function updatePassword(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $passwordUpdate = new PasswordUpdate();

        $user = $this->getUser();

        $form = $this->createForm(PasswordUpdateType::class, $passwordUpdate);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
           //Verifier que le oldPassord du formulaire soit le même mot de passe de l'utilisateur
            if(!password_verify($passwordUpdate->getOldPassword(), $user->getPassword())){
                //gerer l'erreur
                $form->get('oldPassword')->addError(new FormError("Le mot de passe que vous avez tapé n'est pas vorte mot de passe actuel !"));
            } else{
                $newPassword = $passwordUpdate->getNewPassword();
                $hash = $encoder->encodePassword($user, $newPassword);
                $user->setPassword($hash);

                $this->em->persist($user);
                $this->em->flush();
                $this->addFlash(
                    'success',
                    "Votre mot de passe a bien été modifié !"
                );
    
                return $this->redirectToRoute('account');
            }   
        }
        
        return $this->render('account/password.html.twig',[
            'form' => $form->createView()
        ]);
        
    }
}
