<?php

namespace App\Controller\Admin;

use App\Entity\Article;
use App\Entity\FirstPageArticle;
use App\Entity\TagArticle;
use Doctrine\Common\Collections\ArrayCollection;
use PhpParser\Node\Expr\Cast\Object_;
use Symfony\Component\Form\FormTypeInterface;
use App\Form\ArticleType;
use App\Form\FirstPageArticleType;
use App\Entity\ImagesArticle;
use App\Repository\ArticleRepository;
use App\Repository\FirstPageArticleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class AdminArticleController extends AbstractController
{

    private $em;
    private $articleRepository;
    public function __construct(ArticleRepository $articleRepository, FirstPageArticleRepository $firstPageArticleRepository, EntityManagerInterface $em)
    {
        $this->articleRepository = $articleRepository;
        $this->firstPageArticleRepository = $firstPageArticleRepository;
        $this->em = $em;
    }

    /**
     * @Route("/admin/article", name="article_admin_index", methods={"GET"})
     */
    public function index(Request $request, PaginatorInterface $paginator): Response
    {
        $articles = $this->articleRepository->findAll();
        $pagination = $paginator->paginate($articles, $request->query->getInt('page', 1), 12);

        return $this->render('administration/article/index.html.twig', [
            'articles' => $pagination,
        ]);
    }


    /**
     * 
     * @Route("/admin/article/new", name="article_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $article->setSlug(sha1(time().uniqid()));
            $article->setCreatedAt( new \DateTime());
            $article->setUpdatedAt(new \DateTime());
            $entityManager->persist($article);
            $entityManager->flush();

            return $this->redirectToRoute('article_admin_index');
        }

        return $this->render('administration/article/new.html.twig', [
            'article' => $article,
            'form' => $form->createView(),
        ]);
    }

    /**
     * 
     * @Route("/admin/article/{id}/edit", name="article_edit", methods={"GET","POST"})
     */
    public function edit($id, Request $request, EntityManagerInterface $entityManager): Response
    {
        if (null === $article = $entityManager->getRepository(Article::class)->find($id)) {
            throw $this->createNotFoundException('Pas d\'article trouvé pour found l\'id '.$id);
        }
        $originalTags = new ArrayCollection();
        $editForm = $this->createForm(ArticleType::class, $article);
        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            foreach ($article->getTagArticle() as $tag) {
                $originalTags->add($tag);
            }
            $article->setUpdatedAt(new \DateTime());
            $article->setTagArticle($originalTags);
            foreach ($originalTags as $tag) {
                $tag->getArticles()->removeElement($article);
                $tag->setArticles(array($article));
                $this->getDoctrine()->getManager()->persist($tag);
            }
            foreach ($article->getImagesArticles() as $image) {
                $image->setArticle($article);
                $this->getDoctrine()->getManager()->persist($image);
            }
             $this->getDoctrine()->getManager()->persist($article);
             $this->getDoctrine()->getManager()->flush();
             return $this->redirectToRoute('article_admin_index');
        }

            return $this->render('administration/article/edit.html.twig', [
                'article' => $article,
                'form' => $editForm->createView(),
            ]);
        }


    /**
     * isGranted("ROLE_SUPER_ADMIN")
     * @Route("/admin/article/{id}/delete", name="article_delete")
     */
    public function delete(Request $request, Article $article): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $fpArticle = $entityManager->getRepository(FirstPageArticle::class)->find(1);
        if ($fpArticle->getArticle1() && $fpArticle->getArticle1()->getId() == $article->getId()) {
            $fpArticle->setArticle1(null);
        }
        if ($fpArticle->getArticle2() && $fpArticle->getArticle2()->getId() == $article->getId()) {
            $fpArticle->setArticle2(null);
        }
        if ($fpArticle->getArticle3() && $fpArticle->getArticle3()->getId() == $article->getId()) {
            $fpArticle->setArticle3(null);
        }
        if ($fpArticle->getArticle4() && $fpArticle->getArticle4()->getId() == $article->getId()) {
            $fpArticle->setArticle4(null);
        }
        if ($fpArticle->getArticle5() && $fpArticle->getArticle5()->getId() == $article->getId()) {
            $fpArticle->setArticle5(null);
        }
        if ($fpArticle->getArticle6() && $fpArticle->getArticle6()->getId() == $article->getId()) {
            $fpArticle->setArticle6(null);
        }
        $entityManager->flush();
        $entityManager->remove($article);
        $entityManager->flush();


        return $this->redirectToRoute('article_index');
    }
    /**
     * isGranted("ROLE_SUPER_ADMIN")
     * @Route("/admin/article/firstpage", name="article_firstpage", methods={"GET","POST"})
     */
    public function firstPage(Request $request): Response
    {
        $firstpage =$this->firstPageArticleRepository->findLast();
        if(!$firstpage){
            $firstpage = new FirstPageArticle;
            $this->getDoctrine()->getManager()->persist($firstpage);
            $this->getDoctrine()->getManager()->flush();
        }
        $form = $this->createForm(FirstPageArticleType::class, $firstpage);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
        }
        return $this->render('administration/article/firstpage.html.twig', [
            'firstpage' => $firstpage,
            'form' => $form->createView(),
        ]);
    } 

     /**
     * isGranted("ROLE_SUPER_ADMIN")
     * @Route("/admin/article/{id}/image", name="article_image_remove")
     */
     public function removeImage(Request $request, ImagesArticle $image): Response
     {

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($image);
        $entityManager->flush();
        return new JsonResponse('');
        // return $this->render('administration/article/firstpage.html.twig', [
        //     'firstpage' => $firstpage,
        //     'form' => $form->createView(),
        // ]);
    }
}
