<?php

namespace App\Controller\Admin;

use App\Entity\Blog;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\FormTypeInterface;
use App\Form\BlogType;
use App\Entity\ImagesBlog;
use App\Repository\BlogRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class AdminBlogController extends AbstractController
{

    private $em;
    private $blogRepository;
    public function __construct(BlogRepository $blogRepository, EntityManagerInterface $em)
    {
        $this->blogRepository = $blogRepository;
        $this->em = $em;
    }

    /**
     * @Route("/admin/blog", name="blog_admin_index", methods={"GET"})
     */
    public function index(Request $request, PaginatorInterface $paginator): Response
    {
        $blogs = $this->blogRepository->findAll();
        $pagination = $paginator->paginate($blogs, $request->query->getInt('page', 1), 12);

        return $this->render('administration/blog/index.html.twig', [
            'blogs' => $pagination,
        ]);
    }


    /**
     * 
     * @Route("/admin/blog/new", name="blog_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $blog = new blog();
        $form = $this->createForm(BlogType::class, $blog);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $blog->setSlug(sha1(time().uniqid()));
            $blog->setCreatedAt( new \DateTime());
            $blog->setUpdatedAt(new \DateTime());
            $entityManager->persist($blog);
            $entityManager->flush();

            return $this->redirectToRoute('blog_admin_index');
        }

        return $this->render('administration/blog/new.html.twig', [
            'blog' => $blog,
            'form' => $form->createView(),
        ]);
    }

    /**
     * 
     * @Route("/admin/blog/{id}/edit", name="blog_edit", methods={"GET","POST"})
     */
    public function edit($id, Request $request, EntityManagerInterface $entityManager): Response
    {
        if (null === $blog = $entityManager->getRepository(Blog::class)->find($id)) {
            throw $this->createNotFoundException('Pas d\'article trouvé pour found l\'id '.$id);
        }
        $originalTags = new ArrayCollection();
        $editform = $this->createForm(BlogType::class, $blog);
        $editform->handleRequest($request);
        if ($editform->isSubmitted() && $editform->isValid())
        {
            foreach ($blog->getTagBlog() as $tag)
            {
                $originalTags->add($tag);
            }
            $blog->setUpdatedAt(new \DateTime());
            $blog->setTagBlog($originalTags);
            foreach ($originalTags as $tag)
            {
                $tag->getBlogs()->removeElement($blog);
                $tag->setBlogs(array($blog));
                $this->getDoctrine()->getManager()->persist($tag);
            }
            $this->getDoctrine()->getManager()->persist($blog);
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('blog_index');
        }

        return $this->render('administration/blog/edit.html.twig', [
            'blog' => $blog,
            'form' => $editform->createView(),
        ]);
    }

    /**
     * isGranted("ROLE_SUPER_ADMIN")
     * @Route("/admin/blog/{id}/delete", name="blog_delete")
     */
    public function delete(Request $request, blog $blog): Response
    {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($blog);
            $entityManager->flush();
        

        return $this->redirectToRoute('blog_index');
    }
}
