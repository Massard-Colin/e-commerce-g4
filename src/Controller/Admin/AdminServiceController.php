<?php

namespace App\Controller\Admin;

use App\Entity\Service;
use App\Entity\Devis;
use Symfony\Component\Form\FormTypeInterface;
use App\Form\ServiceType;
use App\Form\DevisType;
use App\Repository\ServiceRepository;
use App\Repository\DevisRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class AdminServiceController extends AbstractController
{

    private $em;
    private $serviceRepository;
    private $devisRepository;
    public function __construct(ServiceRepository $serviceRepository,DevisRepository $devisRepository,  EntityManagerInterface $em)
    {
        $this->devisRepository = $devisRepository;
        $this->serviceRepository = $serviceRepository;
        $this->em = $em;
    }

    /**
     * @Route("/admin/service", name="service_admin_index", methods={"GET"})
     */
    public function index(Request $request, PaginatorInterface $paginator): Response
    {
        $services = $this->serviceRepository->findAll();
        $pagination = $paginator->paginate($services, $request->query->getInt('page', 1), 12);

        return $this->render('administration/service/index.html.twig', [
            'services' => $pagination,
        ]);
    }

    /**
     * @Route("/admin/devis", name="service_admin_list", methods={"GET"})
     */
    public function listDevis(Request $request, PaginatorInterface $paginator): Response
    {
        $devis = $this->devisRepository->findAll();
        $pagination = $paginator->paginate($devis, $request->query->getInt('page', 1), 12);

        return $this->render('administration/service/listDevis.html.twig', [
            'devis' => $pagination,
        ]);
    }
    /**
     * @Route("/admin/devis/{id}", name="service_admin_show", methods={"GET"})
     */
    public function showDevis(Request $request, Devis $devis): Response
    {
         $form = $this->createForm(DevisType::class, $devis);

        return $this->render('administration/service/showDevis.html.twig', [
            // 'devis' => $devis,
            'form' => $form->createView(),
        ]);
    }

    /**
     * 
     * @Route("/admin/service/new", name="service_new", methods={"GET","POST"})
     * 
     */
    public function new(Request $request): Response
    {
        $service = new Service();
        $form = $this->createForm(ServiceType::class, $service);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($service);
            $entityManager->flush();

            return $this->redirectToRoute('service_index');
        }

        return $this->render('administration/service/new.html.twig', [
            'service' => $service,
            'form' => $form->createView(),
        ]);
    }

    /**
     * 
     * @Route("/admin/service/{id}/edit", name="service_edit", methods={"GET","POST"})
     * @Security("is_granted('ROLE_ADMIN') and user.getAdmin().getServiceReceived() === true ", message= " Cette annonce ne vous appartient pas, vous ne pouvez pas la modifier")
     */
    public function edit(Request $request, Service $service): Response
    {
        
        $form = $this->createForm(ServiceType::class, $service);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('service_admin_index');
        }
        return $this->render('administration/service/edit.html.twig', [
            'service' => $service,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/service/{id}/delete", name="service_delete")
     */
    public function delete(Request $request, Service $service): Response
    { 
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($service);
            $entityManager->flush();

        return $this->redirectToRoute('service_admin_index');
    }

}
