<?php

namespace App\Controller\Admin;

use App\Repository\HomeRepository;
use App\Repository\UserRepository;
use App\Repository\AdminRepository;
use App\Repository\CommandeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Knp\Component\Pager\PaginatorInterface;
use App\Form\HomeType;
use App\Form\ContactType;
use App\Entity\Home;
use App\Entity\Contact;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class AdminHomeController extends AbstractController
{
    private $HomeRepository;

    public function __construct(HomeRepository $HomeRepository, UserRepository $UserRepository, AdminRepository $AdminRepository, CommandeRepository $commandeRepository)
    {
        $this->HomeRepository = $HomeRepository;
        $this->UserRepository = $UserRepository;
        $this->AdminRepository = $AdminRepository;
        $this->commandeRepository = $commandeRepository;
    }


    /**
     * 
     * @Route("/admin/home/edit", name="home_home_edit", methods={"GET","POST"})
     */
    public function editfirstpage(Request $request): Response
    {   $home = $this->HomeRepository->findLast();
        $form = $this->createForm(HomeType::class, $home);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('home_home_edit');
        }

        return $this->render('administration/adminhome.html.twig', [
            'home' => $home,
            'form' => $form->createView(),
        ]);
    }


    /**
     * 
     * @Route("/admin/contact/edit", name="admin_contact_edit", methods={"GET","POST"})
     */
    public function contact(Request $request): Response
    {
        $contact = $this->HomeRepository->findLast();
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_contact_edit',['id' => $contact->getId()]);
        }

        return $this->render('administration/contact.html.twig', [
            'contact' => $contact,
            'form' => $form->createView(),
        ]);
    }
    /**
     * 
     * @Route("/admin/customer", name="admin_customer", methods={"GET","POST"})
     */
    public function listcustomer(Request $request, PaginatorInterface $paginator): Response
    {
        $users = $this->UserRepository->findAll();
        foreach ($users as $user) {
            if(!$user->isAdmin() ){
                 $customers[] = $user;
            }
        }
        $pagination = $paginator->paginate($customers, $request->query->getInt('page', 1), 12);
        return $this->render('administration/customer.html.twig', [
            'customers' => $pagination,
        ]);
    }
    /**
     * 
     * @Route("/admin/commande", name="admin_commande", methods={"GET","POST"})
     */
    public function listCommande(Request $request, PaginatorInterface $paginator): Response
    {
        $commandes = $this->commandeRepository->findAll();

        $pagination = $paginator->paginate($commandes, $request->query->getInt('page', 1), 12);
        return $this->render('administration/commandes.html.twig', [
            'commandes' => $pagination,
        ]);
    }

}
