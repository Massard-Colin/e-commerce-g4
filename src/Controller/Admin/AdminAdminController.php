<?php

namespace App\Controller\Admin;

use App\Entity\Article;
use App\Entity\User;
use App\Entity\Admin;
use App\Entity\CreateAdmin;
use App\Entity\FirstPageArticle;
use Symfony\Component\Form\FormTypeInterface;
use App\Form\ArticleType;
use App\Form\UserType;
use App\Form\AdminType;
use App\Form\FirstPageArticleType;
use App\Form\CreateAdminType;
use App\Entity\ImagesArticle;
use App\Repository\ArticleRepository;
use App\Repository\FirstPageArticleRepository;
use App\Repository\AdminRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class AdminAdminController extends AbstractController
{

    private $passwordEncoder;
    private $em;
    private $userRepository;
    public function __construct(UserRepository $userRepository, EntityManagerInterface $em, UserPasswordEncoderInterface $passwordEncoder, AdminRepository $adminRepository )
    {
        $this->userRepository = $userRepository;
        $this->adminRepository = $adminRepository;
        $this->em = $em;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * 
     * @Route("/admin/admin", name="admin_admin", methods={"GET","POST"})
     */
    public function listadmin(Request $request, PaginatorInterface $paginator): Response
    {
        $users = $this->userRepository->findAll();
        foreach ($users as $key => $user) 
        {
            if($user->getAdmin() === null)
            {
               unset($users[$key]);
            }
        }
        // $admins = $this->adminRepository->findUser($users);
        $pagination = $paginator->paginate($users, $request->query->getInt('page', 1), 12);
        return $this->render('administration/admin/list.html.twig', [
            'admins' => $pagination,
        ]);
    }
        /**
     * 
     * @Route("/admin/admin/new", name="admin_admin_new", methods={"GET","POST"})
     */
    public function newAdmin(Request $request): Response
    {
        $admin = new CreateAdmin();
        $form = $this->createForm(CreateAdminType::class, $admin);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em = $this->getDoctrine()->getManager();
            $user = new User;
            $admin = new Admin;
            $user->setFirstName($form->get('firstname')->getData());
            $user->setLastName($form->get('lastname')->getData());
            $user->setEmail($form->get('email')->getData());
            $user->setSlug(sha1(time().uniqid()));
            $user->setPassword($this->passwordEncoder->encodePassword($user, $form->get('password')->getData()));

            if( $form->get('superadmin')->getData() == true){
               $user->setRoles(["ROLE_SUPER_ADMIN"]);
               $admin->setSuperAdmin(true);
               $admin->setArticle(true);
               $admin->setBlog(true);
               $admin->setServiceEdit(true);
               $admin->setServiceReceived(true);
               $admin->setPurchaseOrder(true);
               $admin->setSocietyContact(true);
               $admin->setFirstPage(true);

            } else {
                $user->setRoles(["ROLE_ADMIN"]);
                $admin->setSuperAdmin(false);
                $admin->setArticle($form->get('article')->getData());
                $admin->setBlog($form->get('blog')->getData());
                $admin->setServiceEdit($form->get('serviceEdit')->getData());
                $admin->setServiceReceived($form->get('serviceReceived')->getData());
                $admin->setPurchaseOrder($form->get('purchaseOrder')->getData());
                $admin->setSocietyContact($form->get('societyContact')->getData());
                $admin->setFirstPage($form->get('fP')->getData());
            }
            $admin->setUser($user);
            $this->em->persist($user);
            $this->em->persist($admin);
            $this->em->flush();

            return $this->redirectToRoute('admin_admin');
        }

        return $this->render('administration/admin/new.html.twig', [
            'admin' => $admin,
            'form' => $form->createView(),
        ]);
    }

    /**
     * 
     * @Route("/admin/admin/user/{id}/edit", name="admin_admin_user_edit", methods={"GET","POST"})
     */
    public function editUserAdmin(Request $request, User $user): Response
    {
        
        // $user = $this->userRepository->findUserAdmin($admin);
        dump($user);
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // if($form->get('superadmin')->getData() == true){

            // }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_admin');
        }

        return $this->render('administration/admin/editUserAdmin.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * 
     * @Route("/admin/admin/{id}/edit", name="admin_admin_edit", methods={"GET","POST"})
     */
    public function editAdmin(Request $request, Admin $admin): Response
    {
        
        $form = $this->createForm(AdminType::class, $admin);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
              if($form->get('superadmin')->getData() == true ){
                    $admin->getUser()->setRoles(["ROLE_SUPER_ADMIN"]);
                    $admin->setSuperAdmin(true);
                    $admin->setArticle(true);
                    $admin->setBlog(true);
                    $admin->setServiceEdit(true);
                    $admin->setServiceReceived(true);
                    $admin->setPurchaseOrder(true);
                    $admin->setSocietyContact(true);
                    $admin->setFirstPage(true);
              } else{
                    $admin->getUser()->setRoles(["ROLE_ADMIN"]);
                    $admin->setSuperAdmin(false);
                    $admin->setArticle($form->get('article')->getData());
                    $admin->setBlog($form->get('blog')->getData());
                    $admin->setServiceEdit($form->get('serviceEdit')->getData());
                    $admin->setServiceReceived($form->get('serviceReceived')->getData());
                    $admin->setPurchaseOrder($form->get('purchaseOrder')->getData());
                    $admin->setSocietyContact($form->get('societyContact')->getData());
                    $admin->setFirstPage($form->get('firstPage')->getData());
              }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_admin');
        }

        return $this->render('administration/admin/editAdmin.html.twig', [
            'admin' => $admin,
            'form' => $form->createView(),
        ]);
    }

}
