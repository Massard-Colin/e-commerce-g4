<?php

namespace App\Controller;

use App\Entity\FirstPageArticle;
use App\Form\FirstPageArticleType;
use App\Repository\ArticleRepository;
use App\Repository\FirstPageArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/first/page/article")
 */
class FirstPageArticleController extends AbstractController
{
    private $articleRepository;

    public function __construct(ArticleRepository $articleRepository)
    {
        $this->articleRepository = $articleRepository;
    }

    /**
     * @Route("/", name="first_page_article_index", methods={"GET"})
     */
    public function index(FirstPageArticleRepository $firstPageArticleRepository): Response
    {
        return $this->render('first_page_article/index.html.twig', [
            'first_page_articles' => $firstPageArticleRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="first_page_article_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $firstPageArticle = new FirstPageArticle();
        $form = $this->createForm(FirstPageArticleType::class, $firstPageArticle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($firstPageArticle);
            $entityManager->flush();

            return $this->redirectToRoute('first_page_article_index');
        }

        return $this->render('first_page_article/new.html.twig', [
            'first_page_article' => $firstPageArticle,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="first_page_article_show", methods={"GET"})
     */
    public function show(FirstPageArticle $firstPageArticle): Response
    {
        return $this->render('first_page_article/show.html.twig', [
            'first_page_article' => $firstPageArticle,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="first_page_article_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, FirstPageArticle $firstPageArticle): Response
    {
        $form = $this->createForm(FirstPageArticleType::class, $firstPageArticle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('first_page_article_index');
        }

        return $this->render('first_page_article/edit.html.twig', [
            'first_page_article' => $firstPageArticle,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="first_page_article_delete", methods={"DELETE"})
     */
    public function delete(Request $request, FirstPageArticle $firstPageArticle): Response
    {
        if ($this->isCsrfTokenValid('delete'.$firstPageArticle->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($firstPageArticle);
            $entityManager->flush();
        }

        return $this->redirectToRoute('first_page_article_index');
    }
}
