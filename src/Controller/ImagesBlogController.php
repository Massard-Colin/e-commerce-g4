<?php

namespace App\Controller;

use App\Entity\ImagesBlog;
use App\Form\ImagesBlogType;
use App\Repository\ImagesBlogRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/images/blog")
 */
class ImagesBlogController extends AbstractController
{
    /**
     * @Route("/", name="images_blog_index", methods={"GET"})
     */
    public function index(ImagesBlogRepository $imagesBlogRepository): Response
    {
        return $this->render('images_blog/index.html.twig', [
            'images_blogs' => $imagesBlogRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="images_blog_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $imagesBlog = new ImagesBlog();
        $form = $this->createForm(ImagesBlogType::class, $imagesBlog);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($imagesBlog);
            $entityManager->flush();

            return $this->redirectToRoute('images_blog_index');
        }

        return $this->render('images_blog/new.html.twig', [
            'images_blog' => $imagesBlog,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="images_blog_show", methods={"GET"})
     */
    public function show(ImagesBlog $imagesBlog): Response
    {
        return $this->render('images_blog/show.html.twig', [
            'images_blog' => $imagesBlog,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="images_blog_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, ImagesBlog $imagesBlog): Response
    {
        $form = $this->createForm(ImagesBlogType::class, $imagesBlog);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('images_blog_index');
        }

        return $this->render('images_blog/edit.html.twig', [
            'images_blog' => $imagesBlog,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="images_blog_delete", methods={"DELETE"})
     */
    public function delete(Request $request, ImagesBlog $imagesBlog): Response
    {
        if ($this->isCsrfTokenValid('delete'.$imagesBlog->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($imagesBlog);
            $entityManager->flush();
        }

        return $this->redirectToRoute('images_blog_index');
    }
}
