<?php

namespace App\Controller;

use App\Entity\Blog;
use App\Form\BlogType;
use App\Repository\BlogRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;

class BlogController extends AbstractController
{
    private $em;
    private $blogRepository;
    public function __construct(blogRepository $blogRepository, EntityManagerInterface $em)
    {
        $this->blogRepository = $blogRepository;
        $this->em = $em;
    }

    /**
     * @Route("/blog", name="blog_index", methods={"GET"})
     */
    public function index(Request $request, PaginatorInterface $paginator): Response
    {
        $blogs = $this->blogRepository->findAll();
        $dernierBlogs = $this->blogRepository->findByLastBlogs();
        $pagination = $paginator->paginate($blogs, $request->query->getInt('page', 1), 12);

        return $this->render('blog/index.html.twig', [
            'blogs' => $pagination,
        ]);
    }

    /**
     * @Route("/blog/new", name="blog_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $blog = new Blog();
        $form = $this->createForm(BlogType::class, $blog);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($blog);
            $entityManager->flush();

            return $this->redirectToRoute('blog_index');
        }

        return $this->render('blog/new.html.twig', [
            'blog' => $blog,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/blog/{id}", name="blog_show", methods={"GET"})
     */
    public function show(Blog $blog): Response
    {
        $relationsBlog = null;
        if ($blog->getTagBlog()->first())
        {
            $category = $blog->getTagBlog()->first();
            $relationsBlog = $this->blogRepository->findRelation($category);
        }
        return $this->render('blog/show.html.twig', [
            'blog' => $blog,
            'relationsBlog' => $relationsBlog
        ]);
    }

    /**
     * @Route("/blog/edit/{id}", name="blog_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Blog $blog): Response
    {
        $form = $this->createForm(BlogType::class, $blog);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('blog_index');
        }

        return $this->render('blog/edit.html.twig', [
            'blog' => $blog,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/blog/{id}/delete", name="blog_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Blog $blog): Response
    {
        if ($this->isCsrfTokenValid('delete'.$blog->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($blog);
            $entityManager->flush();
        }

        return $this->redirectToRoute('blog_index');
    }
}
