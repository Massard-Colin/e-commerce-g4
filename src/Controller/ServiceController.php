<?php

namespace App\Controller;

use App\Entity\Service;
use App\Form\ServiceType;
use App\Repository\ServiceRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class ServiceController extends AbstractController
{

    private $serviceRepository;

    public function __construct(ServiceRepository $serviceRepository)
    {
        $this->serviceRepository = $serviceRepository;
    }

    /**
     * @Route("/service", name="service_index", methods={"GET"})
     *
     */
    /*@Route("/admin/service", name="service_index", methods={"GET"})*/
    public function index(ServiceRepository $serviceRepository, PaginatorInterface $paginator, Request $request): Response
    {
        $services = $this->serviceRepository->findAll();
        $pagination = $paginator->paginate($services, $request->query->getInt('page', 1),6);

        return $this->render('service/index.html.twig', [
            'services' => $pagination,
        ]);
    }

    /**
     * @Route("/service/{id}", name="service_show", methods={"GET"})
     */
    public function show($id): Response
    {
        $service = $this->serviceRepository->findOneBy(['id' => $id]);
        return $this->render('service/show.html.twig', [
            'service' => $service,
        ]);
    }
}
