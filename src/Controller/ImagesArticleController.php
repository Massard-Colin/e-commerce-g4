<?php

namespace App\Controller;

use App\Entity\ImagesArticle;
use App\Form\ImagesArticleType;
use App\Repository\ImagesArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/images/article")
 */
class ImagesArticleController extends AbstractController
{
    /**
     * @Route("/", name="images_article_index", methods={"GET"})
     */
    public function index(ImagesArticleRepository $imagesArticleRepository): Response
    {
        return $this->render('images_article/index.html.twig', [
            'images_articles' => $imagesArticleRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="images_article_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $imagesArticle = new ImagesArticle();
        $form = $this->createForm(ImagesArticleType::class, $imagesArticle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($imagesArticle);
            $entityManager->flush();

            return $this->redirectToRoute('images_article_index');
        }

        return $this->render('images_article/new.html.twig', [
            'images_article' => $imagesArticle,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="images_article_show", methods={"GET"})
     */
    public function show(ImagesArticle $imagesArticle): Response
    {
        return $this->render('images_article/show.html.twig', [
            'images_article' => $imagesArticle,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="images_article_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, ImagesArticle $imagesArticle): Response
    {
        $form = $this->createForm(ImagesArticleType::class, $imagesArticle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('images_article_index');
        }

        return $this->render('images_article/edit.html.twig', [
            'images_article' => $imagesArticle,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="images_article_delete", methods={"DELETE"})
     */
    public function delete(Request $request, ImagesArticle $imagesArticle): Response
    {
        if ($this->isCsrfTokenValid('delete'.$imagesArticle->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($imagesArticle);
            $entityManager->flush();
        }

        return $this->redirectToRoute('images_article_index');
    }
}
