<?php

namespace App\Controller;

use App\Entity\Commande;
use App\Form\CommandeType;
use App\Entity\User;
use App\Repository\CommandeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class CommandeController extends AbstractController
{
    /**
     * @Route("/commande", name="commande_index", methods={"GET"})
     *
     */
    public function index(CommandeRepository $commandeRepository): Response
    {
        $commandes = $this->getUser()->getCommandes();
        return $this->render('commande/index.html.twig', [
            'commandes' => $commandes,
        ]);
    }

    /**
     * @Route("/commande/show", name="commande_show", methods={"GET"})
     */
    public function show(): Response
    {
        return $this->render('commande/show.html.twig', [
            'commandes' => $commandes,
        ]);
    }

    /**
     * @Route("/commande/recap", name="commande_recap", methods={"GET"})
     */
    public function recapCommande(): Response
    {
        return $this->render('commande/recap.html.twig', [
        ]);
    }

    /**
     * @Route("/commande/validated", name="commande_validated", methods={"GET"})
     */
    public function validatedCommande(): Response
    {
        return $this->render('commande/validated.html.twig', [
        ]);
    }

    /**
     * @Route("/commande/history", name="commande_history", methods={"GET"})
     */
    public function historyCommande(): Response
    {
        return $this->render('commande/history.html.twig', [
        ]);
    }
    public function findUser() {
        $this->em->getRepository(Commande::Class)->findUserCommande($user);
        $userCmd =$this->em->getRepository(Commande::Class)->findUserCommande($user);
    }
}
