<?php

namespace App\Controller;

use App\Repository\BlogRepository;
use App\Repository\ArticleRepository;
use App\Entity\FirstPageArticle;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;

class HomeController extends AbstractController
{
    private $blogRepository;
    private $articleRepository;
    private $em;
    public function __construct(blogRepository $blogRepository, ArticleRepository $articleRepository, EntityManagerInterface $em)
    {
        $this->blogRepository = $blogRepository;
        $this->articleRepository = $articleRepository;
        $this->em = $em;
    }

    /**
     * @Route("/", name="home")
     */
    public function index()
    {

        $firstPageArticle = $this->em->getRepository(FirstPageArticle::class)->findLast();
        $article1 = $firstPageArticle->getArticle1();
        $article2 = $firstPageArticle->getArticle2();
        $article3 = $firstPageArticle->getArticle3();
        $article4 = $firstPageArticle->getArticle4();
        $article5 = $firstPageArticle->getArticle5();
        $article6 = $firstPageArticle->getArticle6();

    	$blogs = $this->blogRepository->findByLastBLogs();

    	return $this->render('home.html.twig', [
    		'blogs' => $blogs,
            'article1' => $firstPageArticle->getArticle1(),
            'article2' => $firstPageArticle->getArticle2(),
            'article3' => $firstPageArticle->getArticle3(),
            'article4' => $firstPageArticle->getArticle4(),
            'article5' => $firstPageArticle->getArticle5(),
            'article6' => $firstPageArticle->getArticle6(),
    	]);
    }

    /**
     * @Route("/styleguide", name="styleguide")
     */
    public function style()
    {
        return $this->render('styleguide.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }

    /**
     * @Route("/contact", name="contact")
     */
    public function contact()
    {
        return $this->render('contact/index.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }

    /**
     * @Route("/rgpd", name="rgpd")
     */
    public function goRgpd()
    {
        return $this->render('page/rgpd.html.twg', [
        ]);
    }
}
