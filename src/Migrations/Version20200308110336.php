<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200308110336 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE first_page_article ADD article1_id INT DEFAULT NULL, ADD article2_id INT DEFAULT NULL, ADD article3_id INT DEFAULT NULL, ADD article5_id INT DEFAULT NULL, DROP article1, DROP article2, DROP article3, DROP article4, DROP article5, CHANGE article6_id article6_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE first_page_article ADD CONSTRAINT FK_8DAEB3622FA7667A FOREIGN KEY (article1_id) REFERENCES article (id)');
        $this->addSql('ALTER TABLE first_page_article ADD CONSTRAINT FK_8DAEB3623D12C994 FOREIGN KEY (article2_id) REFERENCES article (id)');
        $this->addSql('ALTER TABLE first_page_article ADD CONSTRAINT FK_8DAEB36285AEAEF1 FOREIGN KEY (article3_id) REFERENCES article (id)');
        $this->addSql('ALTER TABLE first_page_article ADD CONSTRAINT FK_8DAEB362A0C5F12D FOREIGN KEY (article5_id) REFERENCES article (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8DAEB3622FA7667A ON first_page_article (article1_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8DAEB3623D12C994 ON first_page_article (article2_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8DAEB36285AEAEF1 ON first_page_article (article3_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8DAEB362A0C5F12D ON first_page_article (article5_id)');
        $this->addSql('ALTER TABLE admin CHANGE user_id user_id INT DEFAULT NULL, CHANGE super_admin super_admin TINYINT(1) DEFAULT NULL, CHANGE article article TINYINT(1) DEFAULT NULL, CHANGE blog blog TINYINT(1) DEFAULT NULL, CHANGE service_edit service_edit TINYINT(1) DEFAULT NULL, CHANGE first_page first_page TINYINT(1) DEFAULT NULL, CHANGE service_received service_received TINYINT(1) DEFAULT NULL, CHANGE purchase_order purchase_order TINYINT(1) DEFAULT NULL, CHANGE society_contact society_contact TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE article CHANGE file_name_image file_name_image VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE commande CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE commande_line CHANGE article_id article_id INT DEFAULT NULL, CHANGE commande_id commande_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE connection CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE contact CHANGE address address VARCHAR(255) DEFAULT NULL, CHANGE zip zip INT DEFAULT NULL, CHANGE city city VARCHAR(255) DEFAULT NULL, CHANGE open open TIME DEFAULT NULL, CHANGE close close TIME DEFAULT NULL, CHANGE email email VARCHAR(255) DEFAULT NULL, CHANGE phone phone VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE devis CHANGE service_id service_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE home CHANGE title title VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE images_article CHANGE article_id article_id INT DEFAULT NULL, CHANGE file_name_image file_name_image VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE images_blog CHANGE blog_id blog_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE shopping_cart_line CHANGE article_id article_id INT DEFAULT NULL, CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE admin_id admin_id INT DEFAULT NULL, CHANGE civility civility SMALLINT DEFAULT NULL, CHANGE date_birth date_birth DATE DEFAULT NULL, CHANGE address address VARCHAR(255) DEFAULT NULL, CHANGE cp cp INT DEFAULT NULL, CHANGE city city VARCHAR(255) DEFAULT NULL, CHANGE country country VARCHAR(255) DEFAULT NULL, CHANGE roles roles JSON DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE admin CHANGE user_id user_id INT DEFAULT NULL, CHANGE super_admin super_admin TINYINT(1) DEFAULT \'NULL\', CHANGE article article TINYINT(1) DEFAULT \'NULL\', CHANGE blog blog TINYINT(1) DEFAULT \'NULL\', CHANGE service_edit service_edit TINYINT(1) DEFAULT \'NULL\', CHANGE first_page first_page TINYINT(1) DEFAULT \'NULL\', CHANGE service_received service_received TINYINT(1) DEFAULT \'NULL\', CHANGE purchase_order purchase_order TINYINT(1) DEFAULT \'NULL\', CHANGE society_contact society_contact TINYINT(1) DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE article CHANGE file_name_image file_name_image VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE commande CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE commande_line CHANGE article_id article_id INT DEFAULT NULL, CHANGE commande_id commande_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE connection CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE contact CHANGE address address VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE zip zip INT DEFAULT NULL, CHANGE city city VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE open open TIME DEFAULT \'NULL\', CHANGE close close TIME DEFAULT \'NULL\', CHANGE email email VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE phone phone VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE devis CHANGE service_id service_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE first_page_article DROP FOREIGN KEY FK_8DAEB3622FA7667A');
        $this->addSql('ALTER TABLE first_page_article DROP FOREIGN KEY FK_8DAEB3623D12C994');
        $this->addSql('ALTER TABLE first_page_article DROP FOREIGN KEY FK_8DAEB36285AEAEF1');
        $this->addSql('ALTER TABLE first_page_article DROP FOREIGN KEY FK_8DAEB362A0C5F12D');
        $this->addSql('DROP INDEX UNIQ_8DAEB3622FA7667A ON first_page_article');
        $this->addSql('DROP INDEX UNIQ_8DAEB3623D12C994 ON first_page_article');
        $this->addSql('DROP INDEX UNIQ_8DAEB36285AEAEF1 ON first_page_article');
        $this->addSql('DROP INDEX UNIQ_8DAEB362A0C5F12D ON first_page_article');
        $this->addSql('ALTER TABLE first_page_article ADD article1 INT NOT NULL, ADD article2 INT NOT NULL, ADD article3 INT NOT NULL, ADD article4 INT NOT NULL, ADD article5 INT NOT NULL, DROP article1_id, DROP article2_id, DROP article3_id, DROP article5_id, CHANGE article6_id article6_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE home CHANGE title title VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE images_article CHANGE article_id article_id INT DEFAULT NULL, CHANGE file_name_image file_name_image VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE images_blog CHANGE blog_id blog_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE shopping_cart_line CHANGE article_id article_id INT DEFAULT NULL, CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE admin_id admin_id INT DEFAULT NULL, CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_bin`, CHANGE civility civility SMALLINT DEFAULT NULL, CHANGE date_birth date_birth DATE DEFAULT \'NULL\', CHANGE address address VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE cp cp INT DEFAULT NULL, CHANGE city city VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE country country VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
    }
}
