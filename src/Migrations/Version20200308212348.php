<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200308212348 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE blog ADD file_name_image VARCHAR(255) DEFAULT NULL, DROP cover_image');
        $this->addSql('ALTER TABLE images_article CHANGE article_id article_id INT DEFAULT NULL, CHANGE file_name_image file_name_image VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE admin CHANGE user_id user_id INT DEFAULT NULL, CHANGE super_admin super_admin TINYINT(1) DEFAULT NULL, CHANGE article article TINYINT(1) DEFAULT NULL, CHANGE blog blog TINYINT(1) DEFAULT NULL, CHANGE service_edit service_edit TINYINT(1) DEFAULT NULL, CHANGE first_page first_page TINYINT(1) DEFAULT NULL, CHANGE service_received service_received TINYINT(1) DEFAULT NULL, CHANGE purchase_order purchase_order TINYINT(1) DEFAULT NULL, CHANGE society_contact society_contact TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE article CHANGE file_name_image file_name_image VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE commande CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE commande_line CHANGE article_id article_id INT DEFAULT NULL, CHANGE commande_id commande_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE connection CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE contact CHANGE address address VARCHAR(255) DEFAULT NULL, CHANGE zip zip INT DEFAULT NULL, CHANGE city city VARCHAR(255) DEFAULT NULL, CHANGE open open TIME DEFAULT NULL, CHANGE close close TIME DEFAULT NULL, CHANGE email email VARCHAR(255) DEFAULT NULL, CHANGE phone phone VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE devis CHANGE service_id service_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE first_page_article CHANGE article1_id article1_id INT DEFAULT NULL, CHANGE article2_id article2_id INT DEFAULT NULL, CHANGE article3_id article3_id INT DEFAULT NULL, CHANGE article4_id article4_id INT DEFAULT NULL, CHANGE article5_id article5_id INT DEFAULT NULL, CHANGE article6_id article6_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE home CHANGE title title VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE images_blog CHANGE blog_id blog_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE shopping_cart_line CHANGE article_id article_id INT DEFAULT NULL, CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE admin_id admin_id INT DEFAULT NULL, CHANGE civility civility SMALLINT DEFAULT NULL, CHANGE date_birth date_birth DATE DEFAULT NULL, CHANGE address address VARCHAR(255) DEFAULT NULL, CHANGE cp cp INT DEFAULT NULL, CHANGE city city VARCHAR(255) DEFAULT NULL, CHANGE country country VARCHAR(255) DEFAULT NULL, CHANGE roles roles JSON DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE admin CHANGE user_id user_id INT DEFAULT NULL, CHANGE super_admin super_admin TINYINT(1) DEFAULT \'NULL\', CHANGE article article TINYINT(1) DEFAULT \'NULL\', CHANGE blog blog TINYINT(1) DEFAULT \'NULL\', CHANGE service_edit service_edit TINYINT(1) DEFAULT \'NULL\', CHANGE first_page first_page TINYINT(1) DEFAULT \'NULL\', CHANGE service_received service_received TINYINT(1) DEFAULT \'NULL\', CHANGE purchase_order purchase_order TINYINT(1) DEFAULT \'NULL\', CHANGE society_contact society_contact TINYINT(1) DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE article CHANGE file_name_image file_name_image VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE blog ADD cover_image VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, DROP file_name_image');
        $this->addSql('ALTER TABLE commande CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE commande_line CHANGE article_id article_id INT DEFAULT NULL, CHANGE commande_id commande_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE connection CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE contact CHANGE address address VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE zip zip INT DEFAULT NULL, CHANGE city city VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE open open TIME DEFAULT \'NULL\', CHANGE close close TIME DEFAULT \'NULL\', CHANGE email email VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE phone phone VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE devis CHANGE service_id service_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE first_page_article CHANGE article1_id article1_id INT DEFAULT NULL, CHANGE article2_id article2_id INT DEFAULT NULL, CHANGE article3_id article3_id INT DEFAULT NULL, CHANGE article4_id article4_id INT DEFAULT NULL, CHANGE article5_id article5_id INT DEFAULT NULL, CHANGE article6_id article6_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE home CHANGE title title VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE images_article CHANGE article_id article_id INT DEFAULT NULL, CHANGE file_name_image file_name_image VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE images_blog CHANGE blog_id blog_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE shopping_cart_line CHANGE article_id article_id INT DEFAULT NULL, CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE admin_id admin_id INT DEFAULT NULL, CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_bin`, CHANGE civility civility SMALLINT DEFAULT NULL, CHANGE date_birth date_birth DATE DEFAULT \'NULL\', CHANGE address address VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE cp cp INT DEFAULT NULL, CHANGE city city VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE country country VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
    }
}
