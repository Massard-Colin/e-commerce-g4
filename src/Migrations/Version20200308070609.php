<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200308070609 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE category_blog_blog DROP FOREIGN KEY FK_E7A50FE31D383EE9');
        $this->addSql('CREATE TABLE tag_article_article (tag_article_id INT NOT NULL, article_id INT NOT NULL, INDEX IDX_6EC8D627D78F04CD (tag_article_id), INDEX IDX_6EC8D6277294869C (article_id), PRIMARY KEY(tag_article_id, article_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tag_blog_blog (tag_blog_id INT NOT NULL, blog_id INT NOT NULL, INDEX IDX_31F2E7F4E8D1533D (tag_blog_id), INDEX IDX_31F2E7F4DAE07E97 (blog_id), PRIMARY KEY(tag_blog_id, blog_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tag_article_article ADD CONSTRAINT FK_6EC8D627D78F04CD FOREIGN KEY (tag_article_id) REFERENCES tag_article (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tag_article_article ADD CONSTRAINT FK_6EC8D6277294869C FOREIGN KEY (article_id) REFERENCES article (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tag_blog_blog ADD CONSTRAINT FK_31F2E7F4E8D1533D FOREIGN KEY (tag_blog_id) REFERENCES tag_blog (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tag_blog_blog ADD CONSTRAINT FK_31F2E7F4DAE07E97 FOREIGN KEY (blog_id) REFERENCES blog (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE category_blog');
        $this->addSql('DROP TABLE category_blog_blog');
        $this->addSql('DROP TABLE tag');
        $this->addSql('ALTER TABLE blog DROP FOREIGN KEY FK_C0155143BAD26311');
        $this->addSql('DROP INDEX IDX_C0155143BAD26311 ON blog');
        $this->addSql('ALTER TABLE blog DROP tag_id');
        $this->addSql('ALTER TABLE article DROP FOREIGN KEY FK_23A0E66D78F04CD');
        $this->addSql('DROP INDEX IDX_23A0E66D78F04CD ON article');
        $this->addSql('ALTER TABLE article DROP tag_article_id, CHANGE file_name_image file_name_image VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE admin CHANGE user_id user_id INT DEFAULT NULL, CHANGE super_admin super_admin TINYINT(1) DEFAULT NULL, CHANGE article article TINYINT(1) DEFAULT NULL, CHANGE blog blog TINYINT(1) DEFAULT NULL, CHANGE service_edit service_edit TINYINT(1) DEFAULT NULL, CHANGE first_page first_page TINYINT(1) DEFAULT NULL, CHANGE service_received service_received TINYINT(1) DEFAULT NULL, CHANGE purchase_order purchase_order TINYINT(1) DEFAULT NULL, CHANGE society_contact society_contact TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE commande CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE commande_line CHANGE article_id article_id INT DEFAULT NULL, CHANGE commande_id commande_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE connection CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE contact CHANGE address address VARCHAR(255) DEFAULT NULL, CHANGE zip zip INT DEFAULT NULL, CHANGE city city VARCHAR(255) DEFAULT NULL, CHANGE open open TIME DEFAULT NULL, CHANGE close close TIME DEFAULT NULL, CHANGE email email VARCHAR(255) DEFAULT NULL, CHANGE phone phone VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE devis CHANGE service_id service_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE home CHANGE title title VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE images_article CHANGE article_id article_id INT DEFAULT NULL, CHANGE file_name_image file_name_image VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE images_blog CHANGE blog_id blog_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE shopping_cart_line CHANGE article_id article_id INT DEFAULT NULL, CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE admin_id admin_id INT DEFAULT NULL, CHANGE civility civility SMALLINT DEFAULT NULL, CHANGE date_birth date_birth DATE DEFAULT NULL, CHANGE address address VARCHAR(255) DEFAULT NULL, CHANGE cp cp INT DEFAULT NULL, CHANGE city city VARCHAR(255) DEFAULT NULL, CHANGE country country VARCHAR(255) DEFAULT NULL, CHANGE roles roles JSON DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE category_blog (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE category_blog_blog (category_blog_id INT NOT NULL, blog_id INT NOT NULL, INDEX IDX_E7A50FE3DAE07E97 (blog_id), INDEX IDX_E7A50FE31D383EE9 (category_blog_id), PRIMARY KEY(category_blog_id, blog_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE tag (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE category_blog_blog ADD CONSTRAINT FK_E7A50FE31D383EE9 FOREIGN KEY (category_blog_id) REFERENCES category_blog (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE category_blog_blog ADD CONSTRAINT FK_E7A50FE3DAE07E97 FOREIGN KEY (blog_id) REFERENCES blog (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE tag_article_article');
        $this->addSql('DROP TABLE tag_blog_blog');
        $this->addSql('ALTER TABLE admin CHANGE user_id user_id INT DEFAULT NULL, CHANGE super_admin super_admin TINYINT(1) DEFAULT \'NULL\', CHANGE article article TINYINT(1) DEFAULT \'NULL\', CHANGE blog blog TINYINT(1) DEFAULT \'NULL\', CHANGE service_edit service_edit TINYINT(1) DEFAULT \'NULL\', CHANGE first_page first_page TINYINT(1) DEFAULT \'NULL\', CHANGE service_received service_received TINYINT(1) DEFAULT \'NULL\', CHANGE purchase_order purchase_order TINYINT(1) DEFAULT \'NULL\', CHANGE society_contact society_contact TINYINT(1) DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE article ADD tag_article_id INT DEFAULT NULL, CHANGE file_name_image file_name_image VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE article ADD CONSTRAINT FK_23A0E66D78F04CD FOREIGN KEY (tag_article_id) REFERENCES tag_article (id)');
        $this->addSql('CREATE INDEX IDX_23A0E66D78F04CD ON article (tag_article_id)');
        $this->addSql('ALTER TABLE blog ADD tag_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE blog ADD CONSTRAINT FK_C0155143BAD26311 FOREIGN KEY (tag_id) REFERENCES tag_blog (id)');
        $this->addSql('CREATE INDEX IDX_C0155143BAD26311 ON blog (tag_id)');
        $this->addSql('ALTER TABLE commande CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE commande_line CHANGE article_id article_id INT DEFAULT NULL, CHANGE commande_id commande_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE connection CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE contact CHANGE address address VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE zip zip INT DEFAULT NULL, CHANGE city city VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE open open TIME DEFAULT \'NULL\', CHANGE close close TIME DEFAULT \'NULL\', CHANGE email email VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE phone phone VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE devis CHANGE service_id service_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE home CHANGE title title VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE images_article CHANGE article_id article_id INT DEFAULT NULL, CHANGE file_name_image file_name_image VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE images_blog CHANGE blog_id blog_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE shopping_cart_line CHANGE article_id article_id INT DEFAULT NULL, CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE admin_id admin_id INT DEFAULT NULL, CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_bin`, CHANGE civility civility SMALLINT DEFAULT NULL, CHANGE date_birth date_birth DATE DEFAULT \'NULL\', CHANGE address address VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE cp cp INT DEFAULT NULL, CHANGE city city VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE country country VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
    }
}
