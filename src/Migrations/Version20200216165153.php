<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200216165153 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE article ADD updated_at DATETIME NOT NULL, CHANGE article_category_article_id article_category_article_id INT DEFAULT NULL, CHANGE file_name_image file_name_image VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE blog CHANGE blog_category_blog_id blog_category_blog_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE category_article CHANGE article_id article_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE category_blog CHANGE blog_id blog_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE commande CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE devis CHANGE service_id service_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE images_article CHANGE article_id article_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE images_blog CHANGE blog_id blog_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE roles roles JSON DEFAULT NULL');
        $this->addSql('ALTER TABLE admin CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE article_category_article CHANGE article_id article_id INT DEFAULT NULL, CHANGE category_article_id category_article_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE blog_category_blog CHANGE blog_id blog_id INT DEFAULT NULL, CHANGE category_blog_id category_blog_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE commande_line CHANGE article_id article_id INT DEFAULT NULL, CHANGE commande_id commande_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE connection CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE shopping_cart_line CHANGE article_id article_id INT DEFAULT NULL, CHANGE user_id user_id INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE admin CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE article DROP updated_at, CHANGE article_category_article_id article_category_article_id INT DEFAULT NULL, CHANGE file_name_image file_name_image VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE article_category_article CHANGE article_id article_id INT DEFAULT NULL, CHANGE category_article_id category_article_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE blog CHANGE blog_category_blog_id blog_category_blog_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE blog_category_blog CHANGE blog_id blog_id INT DEFAULT NULL, CHANGE category_blog_id category_blog_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE category_article CHANGE article_id article_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE category_blog CHANGE blog_id blog_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE commande CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE commande_line CHANGE article_id article_id INT DEFAULT NULL, CHANGE commande_id commande_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE connection CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE devis CHANGE service_id service_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE images_article CHANGE article_id article_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE images_blog CHANGE blog_id blog_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE shopping_cart_line CHANGE article_id article_id INT DEFAULT NULL, CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_bin`');
    }
}
