<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200223145210 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE article DROP FOREIGN KEY FK_23A0E66A2F0352C');
        $this->addSql('ALTER TABLE blog DROP FOREIGN KEY FK_C0155143693B150F');
        $this->addSql('ALTER TABLE category_blog DROP FOREIGN KEY FK_4B8E2B04DAE07E97');
        $this->addSql('CREATE TABLE category_article_article (category_article_id INT NOT NULL, article_id INT NOT NULL, INDEX IDX_B7611683548AD6E2 (category_article_id), INDEX IDX_B76116837294869C (article_id), PRIMARY KEY(category_article_id, article_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE category_blog_blog (category_blog_id INT NOT NULL, blog_id INT NOT NULL, INDEX IDX_E7A50FE31D383EE9 (category_blog_id), INDEX IDX_E7A50FE3DAE07E97 (blog_id), PRIMARY KEY(category_blog_id, blog_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE category_article_article ADD CONSTRAINT FK_B7611683548AD6E2 FOREIGN KEY (category_article_id) REFERENCES category_article (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE category_article_article ADD CONSTRAINT FK_B76116837294869C FOREIGN KEY (article_id) REFERENCES article (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE category_blog_blog ADD CONSTRAINT FK_E7A50FE31D383EE9 FOREIGN KEY (category_blog_id) REFERENCES category_blog (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE category_blog_blog ADD CONSTRAINT FK_E7A50FE3DAE07E97 FOREIGN KEY (blog_id) REFERENCES blog (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE article_category_article');
        $this->addSql('DROP TABLE blog_category_blog');
        $this->addSql('ALTER TABLE admin CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('DROP INDEX UNIQ_23A0E66A2F0352C ON article');
        $this->addSql('ALTER TABLE article DROP article_category_article_id, CHANGE file_name_image file_name_image VARCHAR(255) DEFAULT NULL');
        $this->addSql('DROP INDEX UNIQ_C0155143693B150F ON blog');
        $this->addSql('ALTER TABLE blog DROP blog_category_blog_id');
        $this->addSql('ALTER TABLE category_article DROP FOREIGN KEY FK_C5E24E187294869C');
        $this->addSql('DROP INDEX UNIQ_C5E24E187294869C ON category_article');
        $this->addSql('ALTER TABLE category_article DROP article_id');
        $this->addSql('DROP INDEX UNIQ_4B8E2B04DAE07E97 ON category_blog');
        $this->addSql('ALTER TABLE category_blog DROP blog_id');
        $this->addSql('ALTER TABLE commande CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE commande_line CHANGE article_id article_id INT DEFAULT NULL, CHANGE commande_id commande_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE connection CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE devis CHANGE service_id service_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE images_article CHANGE article_id article_id INT DEFAULT NULL, CHANGE file_name_image file_name_image VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE images_blog CHANGE blog_id blog_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE shopping_cart_line CHANGE article_id article_id INT DEFAULT NULL, CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE roles roles JSON DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE article_category_article (id INT AUTO_INCREMENT NOT NULL, article_id INT DEFAULT NULL, category_article_id INT DEFAULT NULL, UNIQUE INDEX UNIQ_D2B6356B7294869C (article_id), UNIQUE INDEX UNIQ_D2B6356B548AD6E2 (category_article_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE blog_category_blog (id INT AUTO_INCREMENT NOT NULL, blog_id INT DEFAULT NULL, category_blog_id INT DEFAULT NULL, UNIQUE INDEX UNIQ_3808E168DAE07E97 (blog_id), UNIQUE INDEX UNIQ_3808E1681D383EE9 (category_blog_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE article_category_article ADD CONSTRAINT FK_D2B6356B548AD6E2 FOREIGN KEY (category_article_id) REFERENCES category_article (id)');
        $this->addSql('ALTER TABLE article_category_article ADD CONSTRAINT FK_D2B6356B7294869C FOREIGN KEY (article_id) REFERENCES article (id)');
        $this->addSql('ALTER TABLE blog_category_blog ADD CONSTRAINT FK_3808E1681D383EE9 FOREIGN KEY (category_blog_id) REFERENCES category_blog (id)');
        $this->addSql('ALTER TABLE blog_category_blog ADD CONSTRAINT FK_3808E168DAE07E97 FOREIGN KEY (blog_id) REFERENCES blog (id)');
        $this->addSql('DROP TABLE category_article_article');
        $this->addSql('DROP TABLE category_blog_blog');
        $this->addSql('ALTER TABLE admin CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE article ADD article_category_article_id INT DEFAULT NULL, CHANGE file_name_image file_name_image VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE article ADD CONSTRAINT FK_23A0E66A2F0352C FOREIGN KEY (article_category_article_id) REFERENCES article_category_article (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_23A0E66A2F0352C ON article (article_category_article_id)');
        $this->addSql('ALTER TABLE blog ADD blog_category_blog_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE blog ADD CONSTRAINT FK_C0155143693B150F FOREIGN KEY (blog_category_blog_id) REFERENCES blog_category_blog (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C0155143693B150F ON blog (blog_category_blog_id)');
        $this->addSql('ALTER TABLE category_article ADD article_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE category_article ADD CONSTRAINT FK_C5E24E187294869C FOREIGN KEY (article_id) REFERENCES article (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C5E24E187294869C ON category_article (article_id)');
        $this->addSql('ALTER TABLE category_blog ADD blog_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE category_blog ADD CONSTRAINT FK_4B8E2B04DAE07E97 FOREIGN KEY (blog_id) REFERENCES blog_category_blog (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_4B8E2B04DAE07E97 ON category_blog (blog_id)');
        $this->addSql('ALTER TABLE commande CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE commande_line CHANGE article_id article_id INT DEFAULT NULL, CHANGE commande_id commande_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE connection CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE devis CHANGE service_id service_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE images_article CHANGE article_id article_id INT DEFAULT NULL, CHANGE file_name_image file_name_image VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE images_blog CHANGE blog_id blog_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE shopping_cart_line CHANGE article_id article_id INT DEFAULT NULL, CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_bin`');
    }
}
