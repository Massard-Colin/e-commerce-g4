<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200308132545 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE article DROP FOREIGN KEY FK_23A0E6618799648');
        $this->addSql('ALTER TABLE article DROP FOREIGN KEY FK_23A0E662FA7667A');
        $this->addSql('ALTER TABLE article DROP FOREIGN KEY FK_23A0E663D12C994');
        $this->addSql('ALTER TABLE article DROP FOREIGN KEY FK_23A0E6685AEAEF1');
        $this->addSql('ALTER TABLE article DROP FOREIGN KEY FK_23A0E66A0C5F12D');
        $this->addSql('ALTER TABLE article DROP FOREIGN KEY FK_23A0E66B2705EC3');
        $this->addSql('DROP INDEX UNIQ_23A0E66A0C5F12D ON article');
        $this->addSql('DROP INDEX UNIQ_23A0E6685AEAEF1 ON article');
        $this->addSql('DROP INDEX UNIQ_23A0E66B2705EC3 ON article');
        $this->addSql('DROP INDEX UNIQ_23A0E662FA7667A ON article');
        $this->addSql('DROP INDEX UNIQ_23A0E6618799648 ON article');
        $this->addSql('DROP INDEX UNIQ_23A0E663D12C994 ON article');
        $this->addSql('ALTER TABLE article DROP article1_id, DROP article2_id, DROP article3_id, DROP article4_id, DROP article5_id, DROP article6_id, CHANGE file_name_image file_name_image VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE first_page_article CHANGE article1_id article1_id INT DEFAULT NULL, CHANGE article2_id article2_id INT DEFAULT NULL, CHANGE article3_id article3_id INT DEFAULT NULL, CHANGE article4_id article4_id INT DEFAULT NULL, CHANGE article5_id article5_id INT DEFAULT NULL, CHANGE article6_id article6_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE admin CHANGE user_id user_id INT DEFAULT NULL, CHANGE super_admin super_admin TINYINT(1) DEFAULT NULL, CHANGE article article TINYINT(1) DEFAULT NULL, CHANGE blog blog TINYINT(1) DEFAULT NULL, CHANGE service_edit service_edit TINYINT(1) DEFAULT NULL, CHANGE first_page first_page TINYINT(1) DEFAULT NULL, CHANGE service_received service_received TINYINT(1) DEFAULT NULL, CHANGE purchase_order purchase_order TINYINT(1) DEFAULT NULL, CHANGE society_contact society_contact TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE commande CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE commande_line CHANGE article_id article_id INT DEFAULT NULL, CHANGE commande_id commande_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE connection CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE contact CHANGE address address VARCHAR(255) DEFAULT NULL, CHANGE zip zip INT DEFAULT NULL, CHANGE city city VARCHAR(255) DEFAULT NULL, CHANGE open open TIME DEFAULT NULL, CHANGE close close TIME DEFAULT NULL, CHANGE email email VARCHAR(255) DEFAULT NULL, CHANGE phone phone VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE devis CHANGE service_id service_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE home CHANGE title title VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE images_article CHANGE article_id article_id INT DEFAULT NULL, CHANGE file_name_image file_name_image VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE images_blog CHANGE blog_id blog_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE shopping_cart_line CHANGE article_id article_id INT DEFAULT NULL, CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE admin_id admin_id INT DEFAULT NULL, CHANGE civility civility SMALLINT DEFAULT NULL, CHANGE date_birth date_birth DATE DEFAULT NULL, CHANGE address address VARCHAR(255) DEFAULT NULL, CHANGE cp cp INT DEFAULT NULL, CHANGE city city VARCHAR(255) DEFAULT NULL, CHANGE country country VARCHAR(255) DEFAULT NULL, CHANGE roles roles JSON DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE admin CHANGE user_id user_id INT DEFAULT NULL, CHANGE super_admin super_admin TINYINT(1) DEFAULT \'NULL\', CHANGE article article TINYINT(1) DEFAULT \'NULL\', CHANGE blog blog TINYINT(1) DEFAULT \'NULL\', CHANGE service_edit service_edit TINYINT(1) DEFAULT \'NULL\', CHANGE first_page first_page TINYINT(1) DEFAULT \'NULL\', CHANGE service_received service_received TINYINT(1) DEFAULT \'NULL\', CHANGE purchase_order purchase_order TINYINT(1) DEFAULT \'NULL\', CHANGE society_contact society_contact TINYINT(1) DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE article ADD article1_id INT DEFAULT NULL, ADD article2_id INT DEFAULT NULL, ADD article3_id INT DEFAULT NULL, ADD article4_id INT DEFAULT NULL, ADD article5_id INT DEFAULT NULL, ADD article6_id INT DEFAULT NULL, CHANGE file_name_image file_name_image VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE article ADD CONSTRAINT FK_23A0E6618799648 FOREIGN KEY (article4_id) REFERENCES first_page_article (id)');
        $this->addSql('ALTER TABLE article ADD CONSTRAINT FK_23A0E662FA7667A FOREIGN KEY (article1_id) REFERENCES first_page_article (id)');
        $this->addSql('ALTER TABLE article ADD CONSTRAINT FK_23A0E663D12C994 FOREIGN KEY (article2_id) REFERENCES first_page_article (id)');
        $this->addSql('ALTER TABLE article ADD CONSTRAINT FK_23A0E6685AEAEF1 FOREIGN KEY (article3_id) REFERENCES first_page_article (id)');
        $this->addSql('ALTER TABLE article ADD CONSTRAINT FK_23A0E66A0C5F12D FOREIGN KEY (article5_id) REFERENCES first_page_article (id)');
        $this->addSql('ALTER TABLE article ADD CONSTRAINT FK_23A0E66B2705EC3 FOREIGN KEY (article6_id) REFERENCES first_page_article (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_23A0E66A0C5F12D ON article (article5_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_23A0E6685AEAEF1 ON article (article3_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_23A0E66B2705EC3 ON article (article6_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_23A0E662FA7667A ON article (article1_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_23A0E6618799648 ON article (article4_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_23A0E663D12C994 ON article (article2_id)');
        $this->addSql('ALTER TABLE commande CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE commande_line CHANGE article_id article_id INT DEFAULT NULL, CHANGE commande_id commande_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE connection CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE contact CHANGE address address VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE zip zip INT DEFAULT NULL, CHANGE city city VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE open open TIME DEFAULT \'NULL\', CHANGE close close TIME DEFAULT \'NULL\', CHANGE email email VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE phone phone VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE devis CHANGE service_id service_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE first_page_article CHANGE article1_id article1_id INT DEFAULT NULL, CHANGE article2_id article2_id INT DEFAULT NULL, CHANGE article3_id article3_id INT DEFAULT NULL, CHANGE article4_id article4_id INT DEFAULT NULL, CHANGE article5_id article5_id INT DEFAULT NULL, CHANGE article6_id article6_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE home CHANGE title title VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE images_article CHANGE article_id article_id INT DEFAULT NULL, CHANGE file_name_image file_name_image VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE images_blog CHANGE blog_id blog_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE shopping_cart_line CHANGE article_id article_id INT DEFAULT NULL, CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE admin_id admin_id INT DEFAULT NULL, CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_bin`, CHANGE civility civility SMALLINT DEFAULT NULL, CHANGE date_birth date_birth DATE DEFAULT \'NULL\', CHANGE address address VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE cp cp INT DEFAULT NULL, CHANGE city city VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE country country VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
    }
}
