<?php

namespace App\DataFixtures;

use Faker\Factory;
//use Cocur\Slugify\Slugify;
use App\Entity\User;
use App\Entity\Admin;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $encoder;


    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {

        $faker = Factory::create('FR-fr');

        $genres = [1,2];
        $bool = [0,1];

        $admin = new admin();
        $admin->setSuperAdmin(1);
        $admin->setArticle(1);
        $admin->setBlog(1);
        $admin->setServiceEdit(1);
        $admin->setFirstPage(1);
        $admin->setServiceReceived(1);
        $admin->setPurchaseOrder(1);
        $admin->setSocietyContact(1);
        $user = new User();
        $user->setRoles(['ROLE_SUPER_ADMIN']);
        $user->setEmail('admin@gmail.fr');
        $user->setPassword($this->encoder->encodePassword($user, 'test'));
        $user->setFirstName($faker->firstName);
        $user->setLastName($faker->lastName);
        $user->setSlug('');
        $user->setCivility($faker->randomElements($genres));
        $user->setDateBirth($faker->dateTimeBetween('-50 years','now'));
        $user->setAddress($faker->streetAddress);
        $user->setCp($faker->postcode);
        $user->setCity($faker->city);
        $user->setCountry($faker->country);
        $admin->setUser($user);

        $manager->persist($user);
        $manager->flush();
        $manager->persist($admin);
        $manager->flush();

        $admin = new admin();
        $admin->setSuperAdmin(0);
        $admin->setArticle($faker->randomElements($bool));
        $admin->setBlog($faker->randomElements($bool));
        $admin->setServiceEdit($faker->randomElements($bool));
        $admin->setFirstPage($faker->randomElements($bool));
        $admin->setServiceReceived($faker->randomElements($bool));
        $admin->setPurchaseOrder($faker->randomElements($bool));
        $admin->setSocietyContact($faker->randomElements($bool));
        $user = new User();
        $user->setRoles(['ROLE_ADMIN']);
        $user->setEmail('admin2@gmail.fr');
        $user->setPassword($this->encoder->encodePassword($user, 'test'));
        $user->setFirstName($faker->firstName);
        $user->setLastName($faker->lastName);
        $user->setSlug('');
        $user->setCivility($faker->randomElements($genres));
        $user->setDateBirth($faker->dateTimeBetween('-50 years','now'));
        $user->setAddress($faker->streetAddress);
        $user->setCp($faker->postcode);
        $user->setCity($faker->city);
        $user->setCountry($faker->country);
        $admin->setUser($user);
        $manager->persist($user);
        $manager->flush();
        $manager->persist($admin);
        $manager->flush();
        
        
        for ($i=1; $i <= 20; $i++) {
            $user = new User();
            $genre = $faker->randomElements($genres);

            $user->setEmail($faker->email);

            $user->setRoles(['ROLE_USER']);

            $user->setPassword($this->encoder->encodePassword($user, 'test'));
            $user->setFirstName($faker->firstName);
            $user->setLastName($faker->lastName);
            $user->setSlug('');
            $user->setEmail($faker->email);
            $user->setCivility($genre);
            $user->setDateBirth($faker->dateTimeBetween('-50 years','now'));
            $user->setAddress($faker->streetAddress);
            $user->setCp($faker->postcode);
            $user->setCity($faker->city);
            $user->setCountry($faker->country);
            $users[] = $user;
            $manager->persist($user);
        }
        $manager->flush();
    }
}