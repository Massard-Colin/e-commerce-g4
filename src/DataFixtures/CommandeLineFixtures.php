<?php

namespace App\DataFixtures;

use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Faker\Factory;
//use Cocur\Slugify\Slugify;
use App\Entity\Article;
use App\Entity\Commande;
use App\Entity\CommandeLine;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class CommandeLineFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('FR-fr');

        $articleRepository = $manager->getRepository(Article::class);
        $commandeRepository = $manager->getRepository(Commande::class);
        $articles = $articleRepository->findAll();
        $commandes = $commandeRepository->findAll();

        for ($i=1; $i <= 500; $i++) {
            $commandeline = new CommandeLine();

            $commandeline->setArticle($faker->randomElement($articles));
            $commandeline->setCommande($faker->randomElement($commandes));
            $commandeline->setStatus($faker->numberBetween(1,4));
            $commandeline->setPrixVente($faker->randomFloat(2,5,100));

            $manager->persist($commandeline);
            $commandeslines[] = $commandeline;

            $manager->flush();
        }

    }

    /**
     * @inheritDoc
     */
    public function getDependencies()
    {
        return [
            ArticleFixtures::class,
            CommandeFixtures::class,
        ];
    }
}