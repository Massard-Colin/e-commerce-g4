<?php

namespace App\DataFixtures;

use App\Entity\TagArticle;
use Faker\Factory;
//use Cocur\Slugify\Slugify;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class TagArticleFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('FR-fr');

        $tagsArticle = [];

        for ($i=1; $i <= 20; $i++) {
            $tagArticle = new TagArticle();

            $tagArticle->setName($faker->word());

            $manager->persist($tagArticle);
            $tagsArticle[] = $tagArticle;

            $manager->flush();
        }
    }
}