<?php

namespace App\DataFixtures;

use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Faker\Factory;
//use Cocur\Slugify\Slugify;
use App\Entity\Devis;
use App\Entity\Service;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class DevisFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('FR-fr');

        $serviceRepository = $manager->getRepository(Service::class);
        $services = $serviceRepository->findAll();

        for ($i=1; $i <= 20; $i++) {
            $devis = new Devis();

            $devis->setService($faker->randomElement($services));
            $devis->setDetails($faker->text(500));
            $devis->setNumContact($faker->numberBetween($min = 1, $max = 10));
            $devis->setEmailContact($faker->email);
            $devis->setCompany($faker->text(15));
            $devis->setLastname($faker->lastName);
            $devis->setFirstname($faker->firstName);
            $devis->setFonction($faker->jobTitle);

            $manager->persist($devis);
            $deviss[] = $devis;

            $manager->flush();
        }
    }

    /**
     * @inheritDoc
     */
    public function getDependencies()
    {
        return [
            ServiceFixture::class,
        ];
    }
}