<?php

namespace App\DataFixtures;

use Faker\Factory;
//use Cocur\Slugify\Slugify;
use App\Entity\Service;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class ServiceFixture extends Fixture
{

    public function load(ObjectManager $manager)
    {

        $faker = Factory::create('FR-fr');

        $services = [];

        for ($i=1; $i <= 25; $i++) {
            $service = new Service();

            $service->setLibelle($faker->jobTitle);
            $service->setContent($faker->paragraph(10));

            $manager->persist($service);
            $services[] = $service;

            $manager->flush();
        }

    }
}