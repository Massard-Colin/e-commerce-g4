<?php

namespace App\DataFixtures;

use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Faker\Factory;
//use Cocur\Slugify\Slugify;
use App\Entity\Article;
use App\Entity\TagArticle;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class ArticleFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('FR-fr');

        $tagArticleRepository = $manager->getRepository(TagArticle::class);
        $tags = $tagArticleRepository->findAll();

        for ($i=1; $i <= 20; $i++) {
            $article = new Article();

            $article->setName($faker->words(2, true));
            $article->setSlug('');
            $article->setPrice($faker->randomFloat(2,5,1000));
            $article->setFileNameImage($faker->imageUrl($width = 640, $height = 480));
            $article->setContent($faker->text(500));
            $article->setQuantity($faker->numberBetween(1,99));
            $article->setCreatedAt($creationDate = $faker->dateTimebetween('-2 years', 'now'));
            $article->setUpdatedAt($faker->dateTimebetween($creationDate, 'now'));
            for($j = 0; $j <= 5; $j++){
                $article->addTag($tags[rand(0,count($tags) - 1)]);
            }

            $manager->persist($article);
            $articles[] = $article;

            $manager->flush();
        }
    }

    /**
     * @inheritDoc
     */
    public function getDependencies()
    {
        return [
            TagArticleFixtures::class,
        ];
    }
}