<?php

namespace App\DataFixtures;

use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Faker\Factory;
//use Cocur\Slugify\Slugify;
use App\Entity\Blog;
use App\Entity\TagBlog;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class BlogFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('FR-fr');

        $tagBlogRepository = $manager->getRepository(TagBlog::class);
        $tags = $tagBlogRepository->findAll();

        for ($i=1; $i <= 30; $i++) {
            $blog = new Blog();

            $blog->setTitle($faker->words(4, true));
            $blog->setContent($faker->text(500));
            $blog->setDescription($faker->text(50));
            $blog->setSlug('');
            $blog->setFileNameImage($faker->imageUrl($width = 640, $height = 480));
            $blog->setCreatedAt($creationDate = $faker->dateTimebetween('-2 years', 'now'));
            $blog->setUpdatedAt($faker->dateTimebetween($creationDate, 'now'));
//            for($j = 0; $j <= 5; $j++){
//                $blog->addTag($tags[rand(0,count($tags) - 1)]);
//            }

            $manager->persist($blog);
            $blogs[] = $blog;

            $manager->flush();
        }
    }

    /**
     * @inheritDoc
     */
    public function getDependencies()
    {
        return [
            TagBlogFixtures::class,
        ];
    }
}