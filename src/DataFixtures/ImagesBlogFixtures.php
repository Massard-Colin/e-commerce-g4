<?php

namespace App\DataFixtures;

use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Faker\Factory;
//use Cocur\Slugify\Slugify;
use App\Entity\Blog;
use App\Entity\ImagesBlog;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class ImagesBlogFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('FR-fr');

        $blogRepository = $manager->getRepository(Blog::class);
        $blogs  = $blogRepository->findAll();

        for($i = 1; $i <= 100; $i++){
            $imageBlog = new ImagesBlog();

            $imageBlog->setBlog($faker->randomElement($blogs));
            $imageBlog->setFileNameImage($faker->imageUrl($width = 640, $height = 480));

            $manager->persist($imageBlog);
            $imageBlogs[] = $imageBlog;

            $manager->flush();
        }
    }

    /**
     * @inheritDoc
     */
    public function getDependencies()
    {
        return [
            BlogFixtures::class,
        ];
    }
}