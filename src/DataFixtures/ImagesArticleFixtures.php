<?php

namespace App\DataFixtures;

use App\Entity\ImagesArticle;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Faker\Factory;
//use Cocur\Slugify\Slugify;
use App\Entity\Article;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class ImagesArticleFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('FR-fr');

        $articleRepository = $manager->getRepository(Article::class);
        $articles = $articleRepository->findAll();

        for($i = 1; $i <= 100; $i++){
            $imageArticle = new ImagesArticle();

            $imageArticle->setArticle($faker->randomElement($articles));
            $imageArticle->setFileNameImage($faker->imageUrl($width = 640, $height = 480));

            $manager->persist($imageArticle);
            $imagesArticle[] = $imageArticle;

            $manager->flush();
        }
    }

    /**
     * @inheritDoc
     */
    public function getDependencies()
    {
        return [
            ArticleFixtures::class,
        ];
    }
}