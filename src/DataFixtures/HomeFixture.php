<?php

namespace App\DataFixtures;

use Faker\Factory;
//use Cocur\Slugify\Slugify;
use App\Entity\Home;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class HomeFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('FR-fr');

        $home = new Home();

        $home->setTitle($faker->text(40));
        $home->setContent($faker->text(500));

        $manager->persist($home);
        $manager->flush();
    }
}