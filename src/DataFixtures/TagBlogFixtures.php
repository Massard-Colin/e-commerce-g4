<?php

namespace App\DataFixtures;

use App\Entity\TagBlog;
use Faker\Factory;
//use Cocur\Slugify\Slugify;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class TagBlogFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('FR-fr');

        for ($i=1; $i <= 20; $i++) {
            $tagBlog = new TagBlog();

            $tagBlog->setName($faker->word());

            $manager->persist($tagBlog);
            $tagsBlog[] = $tagBlog;

            $manager->flush();
        }
    }
}