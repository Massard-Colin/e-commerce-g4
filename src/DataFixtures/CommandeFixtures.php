<?php

namespace App\DataFixtures;

use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Faker\Factory;
//use Cocur\Slugify\Slugify;
use App\Entity\User;
use App\Entity\Commande;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class CommandeFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('FR-fr');

        $userRepository = $manager->getRepository(User::class);
        $users = $userRepository->findAll();

        for ($i=1; $i <= 20; $i++) {
            $commande = new Commande();

            $commande->setUser($faker->randomElement($users));
            $commande->setDate($faker->dateTimeBetween('-1 year','now'));
            $commande->setStatus($faker->numberBetween(1,4));
            $commande->setAddress($faker->streetAddress);
            $commande->setPrixHT($faker->randomFloat(2,5,10000));
            $commande->setMontantTVA($faker->numberBetween(1,20));
            $commande->setRemise($faker->randomFloat(1,0,1));

            $manager->persist($commande);
            $commandes[] = $commande;

            $manager->flush();
        }
    }

    /**
     * @inheritDoc
     */
    public function getDependencies()
    {
        return [
            UserFixtures::class,
        ];
    }
}