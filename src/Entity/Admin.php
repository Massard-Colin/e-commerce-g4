<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Accessible\Annotation\Access;


/**
 * @ORM\Entity(repositoryClass="App\Repository\AdminRepository")
 * 
 */
class Admin
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="admin")
     */
    private $user;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $superAdmin;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $article;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $blog;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $serviceEdit;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $firstPage;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $serviceReceived;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $purchaseOrder;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $societyContact;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getSuperAdmin()
    {
        return $this->superAdmin;
    }

    /**
     * @param mixed $superAdmin
     */
    public function setSuperAdmin($superAdmin): void
    {
        $this->superAdmin = $superAdmin;
    }

    /**
     * @return mixed
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * @param mixed $article
     */
    public function setArticle($article): void
    {
        $this->article = $article;
    }

    /**
     * @return mixed
     */
    public function getBlog()
    {
        return $this->blog;
    }

    /**
     * @param mixed $blog
     */
    public function setBlog($blog): void
    {
        $this->blog = $blog;
    }

    /**
     * @return mixed
     */
    public function getServiceEdit()
    {
        return $this->serviceEdit;
    }

    /**
     * @param mixed $serviceEdit
     */
    public function setServiceEdit($serviceEdit): void
    {
        $this->serviceEdit = $serviceEdit;
    }

    /**
     * @return mixed
     */
    public function getFirstPage()
    {
        return $this->firstPage;
    }

    /**
     * @param mixed $firstPage
     */
    public function setFirstPage($firstPage): void
    {
        $this->firstPage = $firstPage;
    }

    /**
     * @return mixed
     */
    public function getServiceReceived()
    {
        return $this->serviceReceived;
    }

    /**
     * @param mixed $serviceReceived
     */
    public function setServiceReceived($serviceReceived): void
    {
        $this->serviceReceived = $serviceReceived;
    }

    /**
     * @return mixed
     */
    public function getPurchaseOrder()
    {
        return $this->purchaseOrder;
    }

    /**
     * @param mixed $purchaseOrder
     */
    public function setPurchaseOrder($purchaseOrder): void
    {
        $this->purchaseOrder = $purchaseOrder;
    }

    /**
     * @return mixed
     */
    public function getSocietyContact()
    {
        return $this->societyContact;
    }

    /**
     * @param mixed $societyContact
     */
    public function setSocietyContact($societyContact): void
    {
        $this->societyContact = $societyContact;
    }


}
