<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ImagesArticleRepository")
 * @Vich\Uploadable
 */
class ImagesArticle
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

     /**
     * @Vich\UploadableField(mapping="col_article_images", fileNameProperty="fileNameImage")
     * @Assert\Image=(
            mineTypes="image/jpeg"
     )
     * @var File|null
     */
    private $image; 

     /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fileNameImage;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Article", inversedBy="imagesArticles")
     */
    private $article;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image): void
    {
        $this->image = $image;
    }
    /**
     * @return mixed
     */
    public function getFileNameImage()
    {
        return $this->fileNameImage;
    }

    /**
     * @param mixed $fileNameImage
     */
    public function setFileNameImage($fileNameImage): void
    {
        $this->fileNameImage = $fileNameImage;
    }

    /**
     * @return mixed
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * @param mixed $article
     */
    public function setArticle($article): void
    {
        $this->article = $article;
    }

}
