<?php

namespace App\Entity;



class CreateAdmin
{

  private $firstname;
  private $lastname;
  private $email;
  private $password;
  private $superadmin;
  private $article;
  private $blog;
  private $serviceEdit;
  private $fP;
  private $serviceReceived;
  private $purchaseOrder;
  private $societyContact;

    /**
     * @return mixed
     */
    public function getSocietyContact()
    {
        return $this->societyContact;
    }

    /**
     * @param mixed $societyContact
     */
    public function setSocietyContact($societyContact): void
    {
        $this->societyContact = $societyContact;
    }

    /**
     * @return mixed
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param mixed $firstname
     */
    public function setFirstname($firstname): void
    {
        $this->firstname = $firstname;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param mixed $lastname
     */
    public function setLastname($lastname): void
    {
        $this->lastname = $lastname;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password): void
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getSuperadmin()
    {
        return $this->superadmin;
    }

    /**
     * @param mixed $superadmin
     */
    public function setSuperadmin($superadmin): void
    {
        $this->superadmin = $superadmin;
    }

    /**
     * @return mixed
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * @param mixed $article
     */
    public function setArticle($article): void
    {
        $this->article = $article;
    }

    /**
     * @return mixed
     */
    public function getBlog()
    {
        return $this->blog;
    }

    /**
     * @param mixed $blog
     */
    public function setBlog($blog): void
    {
        $this->blog = $blog;
    }

    /**
     * @return mixed
     */
    public function getServiceEdit()
    {
        return $this->serviceEdit;
    }

    /**
     * @param mixed $serviceEdit
     */
    public function setServiceEdit($serviceEdit): void
    {
        $this->serviceEdit = $serviceEdit;
    }

    /**
     * @return mixed
     */
    public function getFP()
    {
        return $this->fP;
    }

    /**
     * @param mixed $fP
     */
    public function setFP($fP): void
    {
        $this->fP = $fP;
    }

    /**
     * @return mixed
     */
    public function getServiceReceived()
    {
        return $this->serviceReceived;
    }

    /**
     * @param mixed $serviceReceived
     */
    public function setServiceReceived($serviceReceived): void
    {
        $this->serviceReceived = $serviceReceived;
    }

    /**
     * @return mixed
     */
    public function getPurchaseOrder()
    {
        return $this->purchaseOrder;
    }

    /**
     * @param mixed $purchaseOrder
     */
    public function setPurchaseOrder($purchaseOrder): void
    {
        $this->purchaseOrder = $purchaseOrder;
    }

}
