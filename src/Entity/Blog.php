<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ORM\Entity(repositoryClass="App\Repository\BlogRepository")
  * @Vich\Uploadable
 */
class Blog
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

     /**
     * @Vich\UploadableField(mapping="blog_images", fileNameProperty="fileNameImage")
     * @Assert\Image=(
            mineTypes="image/jpeg"
     )
     * @var File|null
     */
    private $coverImage;

     /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fileNameImage;

    /**
     * @return mixed
     */
    public function getFileNameImage()
    {
        return $this->fileNameImage;
    }

    /**
     * @param mixed $fileNameImage
     */
    public function setFileNameImage($fileNameImage): void
    {
        $this->fileNameImage = $fileNameImage;
    }

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ImagesBlog", mappedBy="blog")
     */
    private $imagesblogs;

    
     /**
     * @ORM\ManyToMany(targetEntity="App\Entity\TagBlog", mappedBy="blogs", cascade={"persist"})
      * @var Collection
     */
    private $tagBlog;



    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;
    
        /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    public function __construct()
    {
        $this->tagBlog = new ArrayCollection();
    }

    /**
     * @return Collection
     */
    public function getTagBlog(): Collection
    {
        return $this->tagBlog;
    }

    public function addTag(TagBlog $tag): self
    {
        $tag->addBlog($this);
        $this->tagBlog->add($tag);
    }

    /**
    * @param Collection $tagArticle
    */
    public function setTagBlog(Collection $tagBlog): void
    {
        $this->tagBlog = $tagBlog;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt($updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }
    /**
     * @return mixed
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param mixed $tags
     */
    public function setTags($tags): void
    {
        $this->tags = $tags;
    }
    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content): void
    {
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug): void
    {
        $this->slug = $slug;
    }

    /**
     * @return mixed
     */
    public function getCoverImage()
    {
        return $this->coverImage;
    }

    /**
     * @param mixed $coverImage
     */
    public function setCoverImage($coverImage): void
    {
        $this->coverImage = $coverImage;
    }

    /**
     * @return mixed
     */
    public function getImagesblogs()
    {
        return $this->imagesblogs;
    }

    /**
     * @param mixed $imagesblogs
     */
    public function setImagesblogs($imagesblogs): void
    {
        $this->imagesblogs = $imagesblogs;
    }
}
