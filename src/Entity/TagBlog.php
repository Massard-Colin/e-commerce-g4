<?php

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use App\Entity\Article;
use App\Entity\Blog;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;


/**
 * 
 * @ORM\Entity(repositoryClass="App\Repository\TagBlogRepository")
 */
class TagBlog
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    
    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Blog", inversedBy="tagBlog")
     */
    private $blogs;

    public function __toString()
    {
        return $this->name;
    }
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    public function addBlog(Blog $blog)
    {
        if (!$this->blogs->contains($blog))
        {
            $this->blogs->add($blog);
        }
    }

    /**
     * @return mixed
     */
    public function getBlogs()
    {
        return $this->blogs;
    }

    /**
     * @param mixed $blogs
     */
    public function setBlogs($blogs): void
    {
        $this->blogs = $blogs;
    }

}
