<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Accessible\Annotation\Access;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ConnectionRepository")
 */
class Connection
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $connectionDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="connections")
     */
    private $user;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getConnectionDate()
    {
        return $this->connectionDate;
    }

    /**
     * @param mixed $connectionDate
     */
    public function setConnectionDate($connectionDate): void
    {
        $this->connectionDate = $connectionDate;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

}
