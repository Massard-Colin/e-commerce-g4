<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\ArticleRepository")
 * @Vich\Uploadable
 */
class Article
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

     /**
     * @Vich\UploadableField(mapping="article_images", fileNameProperty="fileNameImage")
     * @Assert\Image=(
            mineTypes="image/jpeg"
     )
     * @var File|null
     */
    private $coverImage; 

     /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fileNameImage;


    /**
     * @ORM\Column(type="text")
     */
    private $content;


    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CommandeLine", mappedBy="article")
     */
    private $commandeLines;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ShoppingCartLine", mappedBy="article")
     */
    private $shoppingCartLines;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ImagesArticle", mappedBy="article" , cascade={"persist", "remove"})
     */
    private $imagesArticles;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\TagArticle", mappedBy="articles", cascade={"persist"})
     * @var Collection
     */
    private $tagArticle;


    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    public function __construct()
    {
        $this->tagArticle = new ArrayCollection();
    }



    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt): void
    {
        $this->createdAt = $createdAt;
    }


    /**
     * @return Collection
     */
    public function getTagArticle(): Collection
    {
        return $this->tagArticle;
    }

    public function addTag(TagArticle $tag)
    {
        $tag->addArticle($this);
        $this->tagArticle->add($tag);
    }

    /**
     * @param Collection $tagArticle
     */
    public function setTagArticle(Collection $tagArticle): void
    {
        $this->tagArticle = $tagArticle;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }
    /**
     * @return mixed
     */
    public function getFileNameImage()
    {
        return $this->fileNameImage;
    }

    /**
     * @param mixed $fileNameImage
     */
    public function setFileNameImage($fileNameImage): void
    {
        $this->fileNameImage = $fileNameImage;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug): void
    {
        $this->slug = $slug;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price): void
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getCoverImage()
    {
        return $this->coverImage;
    }

    /**
     * @param mixed $coverImage
     */
    public function setCoverImage(?File $coverImage = null): void
    {
        $this->coverImage = $coverImage;

        // Only change the updated af if the file is really uploaded to avoid database updates.
        // This is needed when the file should be set when loading the entity.
        if ($this->coverImage instanceof UploadedFile) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content): void
    {
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     */
    public function setQuantity($quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return mixed
     */
    public function getCommandeLines()
    {
        return $this->commandeLines;
    }

    /**
     * @param mixed $commandeLines
     */
    public function setCommandeLines($commandeLines): void
    {
        $this->commandeLines = $commandeLines;
    }

    /**
     * @return mixed
     */
    public function getShoppingCartLines()
    {
        return $this->shoppingCartLines;
    }

    /**
     * @param mixed $shoppingCartLines
     */
    public function setShoppingCartLines($shoppingCartLines): void
    {
        $this->shoppingCartLines = $shoppingCartLines;
    }

    /**
     * @return mixed
     */
    public function getImagesArticles()
    {
        return $this->imagesArticles;
    }

    /**
     * @param mixed $imagesArticles
     */
    public function setImagesArticles($imagesArticles): void
    {
        $this->imagesArticles = $imagesArticles;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

}
