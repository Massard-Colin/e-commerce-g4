<?php

namespace App\Entity;



class CatalogueSearch
{

    /**
     * @var int|null 
     */
    private $max;
    /**
     * @var int|null 
     */
    private $min;


    private $tagArticle;

    /**
     * @return mixed
     */
    public function getTagArticle()
    {
        return $this->tagArticle;
    }

    /**
     * @param mixed $tagArticle
     */
    public function setTagArticle($tagArticle): void
    {
        $this->tagArticle = $tagArticle;
    }

    /**
     * @return int|null
     */
    public function getMax(): ?int
    {
        return $this->max;
    }

    /**
     * @param int|null $max
     */
    public function setMax(?int $max): void
    {
        $this->max = $max;
    }

    /**
     * @return int|null
     */
    public function getMin(): ?int
    {
        return $this->min;
    }

    /**
     * @param int|null $min
     */
    public function setMin(?int $min): void
    {
        $this->min = $min;
    }

    /**
     * @return mixed
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param mixed $tags
     */
    public function setTags($tags): void
    {
        $this->tags = $tags;
    }


}
