<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Accessible\Annotation\Access;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FirstPageArticleRepository")
 */
class FirstPageArticle
{
    /**
     * @Access({Access::GET, Access::CALL})
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
       * @ORM\OneToOne(targetEntity="App\Entity\Article")
     */
    private $article1;

    /**
      * @ORM\OneToOne(targetEntity="App\Entity\Article")
     */
    private $article2;

    /**
       * @ORM\OneToOne(targetEntity="App\Entity\Article")
     */
    private $article3;

    /**
     ** @ORM\OneToOne(targetEntity="App\Entity\Article")
     */
    private $article4;

    /**
  *  * @ORM\OneToOne(targetEntity="App\Entity\Article")
     */
    private $article5;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Article")
     */
    private $article6;


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getArticle1()
    {
        return $this->article1;
    }

    /**
     * @param mixed $article1
     */
    public function setArticle1($article1): void
    {
        $this->article1 = $article1;
    }

    /**
     * @return mixed
     */
    public function getArticle2()
    {
        return $this->article2;
    }

    /**
     * @param mixed $article2
     */
    public function setArticle2($article2): void
    {
        $this->article2 = $article2;
    }

    /**
     * @return mixed
     */
    public function getArticle3()
    {
        return $this->article3;
    }

    /**
     * @param mixed $article3
     */
    public function setArticle3($article3): void
    {
        $this->article3 = $article3;
    }

    /**
     * @return mixed
     */
    public function getArticle4()
    {
        return $this->article4;
    }

    /**
     * @param mixed $article4
     */
    public function setArticle4($article4): void
    {
        $this->article4 = $article4;
    }

    /**
     * @return mixed
     */
    public function getArticle5()
    {
        return $this->article5;
    }

    /**
     * @param mixed $article5
     */
    public function setArticle5($article5): void
    {
        $this->article5 = $article5;
    }

    /**
     * @return mixed
     */
    public function getArticle6()
    {
        return $this->article6;
    }

    /**
     * @param mixed $article6
     */
    public function setArticle6($article6): void
    {
        $this->article6 = $article6;
    }

}
