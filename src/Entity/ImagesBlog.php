<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Accessible\Annotation\Access;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ImagesBlogRepository")
  * @Vich\Uploadable
 */
class ImagesBlog
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @Access({Access::GET, Access::CALL})
     * @ORM\Column(type="integer")
     */
    private $id;
     /**
     * @Vich\UploadableField(mapping="blog_col_images", fileNameProperty="fileNameImage")
     * @Assert\Image=(
            mineTypes="image/jpeg"
     )
     * @var File|null
     */
    private $coverImage;

     /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fileNameImage;
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Blog", inversedBy="imagesblogs")
     */
    private $blog;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url): void
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getBlog()
    {
        return $this->blog;
    }

    /**
     * @param mixed $blog
     */
    public function setBlog($blog): void
    {
        $this->blog = $blog;
    }

    /**
     * @return mixed
     */
    public function getFileNameImage()
    {
        return $this->fileNameImage;
    }

    /**
     * @param mixed $fileNameImage
     */
    public function setFileNameImage($fileNameImage): void
    {
        $this->fileNameImage = $fileNameImage;
    }

    /**
     * @return File|null
     */
    public function getCoverImage(): ?File
    {
        return $this->coverImage;
    }

    /**
     * @param File|null $coverImage
     */
    public function setCoverImage(?File $coverImage): void
    {
        $this->coverImage = $coverImage;
    }


}
