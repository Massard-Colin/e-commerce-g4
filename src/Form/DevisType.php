<?php

namespace App\Form;

use App\Entity\Devis;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class DevisType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('service', TextType::class, [
                'attr' => ['readonly' => true],
            ])
            ->add('company', TextType::class, [
                'attr' => ['readonly' => true],
            ])
            ->add('lastname', TextType::class, [
                'attr' => ['readonly' => true],
            ])
            ->add('firstname', TextType::class, [
                'attr' => ['readonly' => true],
            ])
            ->add('fonction', TextType::class, [
                'attr' => ['readonly' => true],
            ])
            ->add('emailContact', TextType::class, [
                'attr' => ['readonly' => true],
            ])
            ->add('numContact', TextType::class, [
                'attr' => ['readonly' => true],
            ])
            ->add('details', TextType::class, [
                'attr' => ['readonly' => true],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Devis::class,
        ]);
    }
}
