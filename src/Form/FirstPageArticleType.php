<?php

namespace App\Form;

use App\Entity\FirstPageArticle;
use App\Entity\Article;
use App\Repository\ArticleRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormTypeInterface;

class FirstPageArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('article1',EntityType::class,[
                'class' => Article::class,
                'query_builder' => function (ArticleRepository $er) {
                    return $er->createQueryBuilder('u')
                    ->orderBy('u.name', 'ASC');
                },
                'choice_label' => 'name',
                'placeholder' => '',

            ])

            ->add('article2',EntityType::class,[
                'class' => Article::class,
                'query_builder' => function (ArticleRepository $er) {
                    return $er->createQueryBuilder('u')
                    ->orderBy('u.name', 'ASC');
                },
                'choice_label' => 'name',
                'placeholder' => '',

            ])


            ->add('article3',EntityType::class,[
                'class' => Article::class,
                'query_builder' => function (ArticleRepository $er) {
                    return $er->createQueryBuilder('u')
                    ->orderBy('u.name', 'ASC');
                },
                'choice_label' => 'name',
                'placeholder' => '',

            ])

            ->add('article4',EntityType::class,[
                'class' => Article::class,
                'query_builder' => function (ArticleRepository $er) {
                    return $er->createQueryBuilder('u')
                    ->orderBy('u.name', 'ASC');
                },
                'choice_label' => 'name',
                'placeholder' => '',

            ])

            ->add('article5',EntityType::class,[
                'class' => Article::class,
                'query_builder' => function (ArticleRepository $er) {
                    return $er->createQueryBuilder('u')
                    ->orderBy('u.name', 'ASC');
                },
                'choice_label' => 'name',
                'placeholder' => '',

            ])

            ->add('article6',EntityType::class,[
                'class' => Article::class,
                'query_builder' => function (ArticleRepository $er) {
                    return $er->createQueryBuilder('u')
                    ->orderBy('u.name', 'ASC');
                },
                'choice_label' => 'name',
                'placeholder' => '',

            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => FirstPageArticle::class,
        ]);
    }
}
