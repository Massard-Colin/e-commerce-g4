<?php

namespace App\Form;

use App\Entity\Blog;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\TagBlog;
use App\Repository\TagBlogRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class BlogType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('title')
        ->add('content')
        ->add('description')
        ->add('coverImage', FileType::class,[
            'required' => false,
        ])
           ->add('tagBlog', EntityType::class, [
                'class' => TagBlog::class,
                'query_builder' => function (TagBlogRepository $er) {
                    return $er->createQueryBuilder('t')
                    ->orderBy('t.name', 'ASC');
                },
                'choice_label' => 'name',
                'label' => 'Ajouter un ou plusieur tag',
                'multiple' => true,
                'attr' => [
                    'class' => 'choice',
                ],
               'required' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Blog::class,
        ]);
    }
}
