<?php

namespace App\Form;

use App\Entity\Article;
use App\Entity\TagArticle;
use App\Repository\ArticleRepository;
use App\Repository\TagArticleRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('price')
            ->add('coverImage', FileType::class,[
                'required' => false,
            ])
            ->add('content')
            ->add('quantity')
            ->add('imagesArticles', CollectionType::class,
                [
                    'entry_type' => ImagesArticleType::class,
                    'allow_add' => true,
                    'prototype' => true,
                    'allow_delete' => true,
                    'by_reference' => true,
                ])
         /* ->add('tagArticle', CollectionType::class,
              [
                  'entry_type' => TagArticleType::class,
                  'allow_add' => true,
                  'prototype' => true,
                  'allow_delete' => true,
              ])*/
//      /*      ->add('tagArticle',CollectionType::class,[
//                'class' => TagArticle::class,
//                'query_builder' => function (TagArticleRepository $er) {
//                    return $er->createQueryBuilder('u')
//                        ->orderBy('u.name', 'ASC');
//                },
//                'choice_label' => 'name',
//                'placeholder' => '-- TAG --',
//
//            ])*/
            ->add('tagArticle', EntityType::class, [
                'class' => TagArticle::class,
                'query_builder' => function (TagArticleRepository $entityRepository) {
                    return $entityRepository->createQueryBuilder('r')->orderBy('r.name', 'ASC');

                },
                'choice_label' => 'name',
                'label' => 'Ajouter un ou plusieur tag',
                'multiple' => true,
                'attr' => [
                    'class' => 'choice',
                ],
                'required' => true,

            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
        ]);
    }
}
