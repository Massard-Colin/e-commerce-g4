<?php

namespace App\Form\Search;

use App\Entity\CatalogueSearch;
use App\Entity\TagArticle;
use App\Repository\TagArticleRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CatalogueSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('max', IntegerType::class, [
                    'required' => false,
                    'label' => false,
                    'attr' => [
                        'placeholder' => 'Prix maximum'],
            ])
            ->add('min', IntegerType::class, [
                    'required' => false,
                    'label' => false,
                    'attr' => [
                        'placeholder' => 'Prix min'],
            ])
            ->add('tagArticle', EntityType::class,[
                      'class' => TagArticle::class,
                      'query_builder' => function (TagArticleRepository $er) {
                        return $er->createQueryBuilder('u')
                        ->orderBy('u.name', 'ASC');
                    },
                      'choice_label' => 'name',
            ]

        )
            ->add('save', SubmitType::class, [
                'label' => 'filtrer',
                    'attr' => [
                        'class' => 'save',

                    ]
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CatalogueSearch::class,
            'method' => 'get',
            'csrf_protection' => false,
        ]);
    }
}
