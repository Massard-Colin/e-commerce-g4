<?php

namespace App\Form;

use App\Entity\Admin;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class AdminType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('superadmin', CheckboxType::class, [
                'label'    => 'Super administrateur',
                'required' => false,
            ])
            ->add('article' , CheckboxType::class, [
                'label'    => 'Création/Supp d\'article',
                'required' => false,
            ])
            ->add('blog' , CheckboxType::class, [
                'label'    => 'Création/Supp d\'article de blog',
                'required' => false,
            ])
            ->add('serviceEdit', CheckboxType::class, [
                'label'    => 'Création/Supp de service',
                'required' => false,
            ])
            ->add('firstPage', CheckboxType::class, [
                'label'    => 'Modification de la première page',
                'required' => false,
            ])
            ->add('serviceReceived', CheckboxType::class, [
                'label'    => 'Gestion des devis',
                'required' => false,
            ])
            ->add('purchaseOrder', CheckboxType::class, [
                'label'    => 'Gestion des commandes',
                'required' => false,
            ])
            ->add('societyContact', CheckboxType::class, [
                'label'    => 'Gestion de la page contact',
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Admin::class,
        ]);
    }
}
