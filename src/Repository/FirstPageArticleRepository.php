<?php

namespace App\Repository;

use App\Entity\FirstPageArticle;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method FirstPageArticle|null find($id, $lockMode = null, $lockVersion = null)
 * @method FirstPageArticle|null findOneBy(array $criteria, array $orderBy = null)
 * @method FirstPageArticle[]    findAll()
 * @method FirstPageArticle[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FirstPageArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FirstPageArticle::class);
    }

    // /**
    //  * @return FirstPageArticle[] Returns an array of FirstPageArticle objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    
    public function findLast(): ?FirstPageArticle
    {
        return $this->createQueryBuilder('f')
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    
}
