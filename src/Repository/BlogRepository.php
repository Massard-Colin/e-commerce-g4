<?php

namespace App\Repository;

use App\Entity\Blog;
use App\Entity\TagBlog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Blog|null find($id, $lockMode = null, $lockVersion = null)
 * @method Blog|null findOneBy(array $criteria, array $orderBy = null)
 * @method Blog[]    findAll()
 * @method Blog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BlogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Blog::class);
    }

    // /**
    //  * @return Blog[] Returns an array of Blog objects
    //  */

    public function findByLastBlogs()
    {
        $query = $this->createQueryBuilder('blog')
            -> orderBy('blog.createdAt','DESC')
            ->setMaxResults(3);
        return $query->getQuery()->execute();
    }
    /*
    public function getBaseQueryBuilder()
    {
        return $this->createQueryBuilder('blog')
            ->getQuery()
            ->getResult()
        ;
    }
    */
    /*
    public function findByLastBlogs()
    {
        $query = $this->createQueryBuilder('blog')
            -> orderBy('blog.createdAt','ASC')
            ->setMaxResults(6);
        return $query->getQuery()->execute();
    }
    */
    /*
    public function findOneBySomeField($value): ?Blog
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findRelation(TagBlog $tagBlog)
    {
        return $this->createQueryBuilder('blog')
            ->leftJoin('blog.tagBlog', 'tagBlog')
            ->andWhere('tagBlog.name = :tag')
            ->andWhere('tagBlog.id IN(:tagId)')
            ->setParameter('tag', $tagBlog->getName())
            ->setParameter('tagId', $tagBlog->getId())
            ->setMaxResults(3)
            ->getQuery()
            ->getResult();
    }
}
