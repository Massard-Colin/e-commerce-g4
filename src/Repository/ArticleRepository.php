<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }

    // public function ArticleSelectionner1($id)
    // {
    //     $query = $this->createQueryBuilder('article')
    //         ->where('article.id = :id' )
    //         ->setParameter('id', $id);
    //     return $query->getQuery()->execute();
    // }
    // public function ArticleSelectionner2($id)
    // {
    //     $query = $this->createQueryBuilder('article')
    //         ->where('article.id = :id' )
    //         ->setParameter('id', $id);
    //     return $query->getQuery()->execute();
    // }
    // public function ArticleSelectionner3($id)
    // {
    //     $query = $this->createQueryBuilder('article')
    //         ->where('article.id = :id' )
    //         ->setParameter('id', $id);
    //     return $query->getQuery()->execute();
    // }
    // public function ArticleSelectionner4($id)
    // {
    //     $query = $this->createQueryBuilder('article')
    //         ->where('article.id = :id' )
    //         ->setParameter('id', $id);
    //     return $query->getQuery()->execute();
    // }
    // public function ArticleSelectionner5($id)
    // {
    //     $query = $this->createQueryBuilder('article')
    //         ->where('article.id = :id' )
    //         ->setParameter('id', $id);
    //     return $query->getQuery()->execute();
    // }
    // public function ArticleSelectionner6($id)
    // {
    //     $query = $this->createQueryBuilder('article')
    //         ->where('article.id = :id' )
    //         ->setParameter('id', $id);
    //     return $query->getQuery()->execute();
    // }


    public function findFilter($min, $max, $tags)
    {
        $query = $this->createQueryBuilder('a')
            ->leftjoin('a.tagArticle', 'tagArticle');

            if(!empty($max)){
                $query = $query->where('a.price BETWEEN :min AND :max');
            }
            if(!empty($tags)){
               $query = $query->andWhere('tagArticle IN (:tags)');
            }


            if ($min) {
                $query = $query->setParameter('min', $min);
            }
            if (!empty($max)) {
                $query = $query->setParameter('max', $max);
            }
            if (!empty($tags)) {
                $query = $query->setParameter('tags', $tags);
            }

            return $query->getQuery()->getResult()
        ;
    }

    /*
     /**
      * @return Article[] Returns an array of Article objects
      */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    // /**
    //  * @return Article[] Returns an array of Article objects
    //  */
    /*public function findByLastArticles()
    {
        $query = $this->createQueryBuilder('article')
            ->orderBy('article.date', 'DESC')
            ->setMaxResults(6);
        return $query->getQuery()->execute();
    }*/

    /*
    public function findOneBySomeField($value): ?Article
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
