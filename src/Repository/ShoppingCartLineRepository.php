<?php

namespace App\Repository;

use App\Entity\ShoppingCartLine;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ShoppingCartLine|null find($id, $lockMode = null, $lockVersion = null)
 * @method ShoppingCartLine|null findOneBy(array $criteria, array $orderBy = null)
 * @method ShoppingCartLine[]    findAll()
 * @method ShoppingCartLine[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShoppingCartLineRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ShoppingCartLine::class);
    }

    // /**
    //  * @return ShoppingCartLine[] Returns an array of ShoppingCartLine objects
    //  */
    
    public function findArticle($article)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.article = :article')
            ->setParameter('article', $article)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findPrice($user)
    {
        return $this->createQueryBuilder('s')
            ->leftjoin('s.article', 'article')->addSelect()
            ->andWhere('s.user = :user')
            ->setParameter('user', $user)
            ->select('SUM(article.price * s.quantity) as total')
            ->groupby('s.user')
            ->getQuery()
            ->getResult()
        ;
    }
    

    /*
    public function findOneBySomeField($value): ?ShoppingCartLine
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
