### E-Commerce G4

Projet e-commerce

## Arborescence CSS 
Pour l'arborescence des fichiers de styles, les fichiers seront séparés en 5 principaux dossier.
*  Components  : Divisé en trois dossier en accord avec l'atomic design : 
    * Atoms : Un élément qui, seul, n’a pas de but fonctionnel. Il est « irréductible » (Exemple liens / button / élement de formulaire)
    * Molécules : Les molécules correspondent à des groupes d’atomes formant une unité (Exemple : carte article)
    * Override-vendors : Dossier servant à surcharger les éléments déjà stylisé venant de différentes librairies.

*  Fonts : Incorpore les différentes polices nécessaire 
*  Helpers : Rassemble les différentes variables sass du projet
*  Fix : Sera ajouté si besoin au projet (Contient les réglage de responsive : cookies / dropdown)

*  Layout : Contient les fichiers relatif à la mise en page principale du site :
    * Base inclut le layout générale de la page html / body / main-wrapper  
    * Container contient les container de base : container container--lg (Même container que sur bootstrap)
    * Fonts intègre les différentes polices du projet
    * Footer pour le design du footer
    * Header pour le design du menu header
    * Typographie contient les différentes style relatives au textes / titres du site

*  Pages : Rassemble le style relatif aux différentes pages lorsque ces derniers n'ont pas leurs place dans le dossier component.

## Nommage BEM 
"BEM est un sigle pour Blocks - Elements - Modifiers. Ce qui en français serait traduit ainsi : Blocs - Éléments - Modificateurs.
Trois entités qui vont permettre de découper et organiser tous les composants d'une page web."
*  Le bloc est un composant «parent» contenant un ou plusieurs éléments. Le bloc peut être indépendant, et lorsque pris hors contexte d'une page spécifique, garde du sens.
    *  par exemple : un menu, un pied de page (footer), un menu latéral (sidebar).
*  L'élément est un composant appartenant à un bloc. Il faut considérer un élément comme l'enfant d'un bloc.
    *  par exemple : le titre d'un bloc, une page d'un menu.
*  Le modificateur va introduire une notion de comportement. Il se modifie en fonction du contexte de la page ou d'une action de l'utilisateur. Le modificateur peut aussi bien être appliqué à un bloc qu'à un élément.
    *  par exemple : un fond de couleur différente pou une page spécifique, un élément rendu visible (ou caché) après un clic de l'utilisateur. 
### GITLAB 
Nommage des commits : Séparé en 3 parties : Type Cible Ficher_Modifié
*  Type : ADD = "Ajout d'un ou plusieurs fichiers" / FIX = "Modifications mineures" / DEL = "Suppression d'un ou plusieurs fichiers" / UP = "Mise à jour d'un ou plusieurs fichiers"
*  Pages concernées / Controller concerné / Fonctionnalité concerné ⚠ A mettre entre []
*  Fichiers Modifié / Fonctionnalité ajouté / Bloc Modifié
### Images 
Taille max des images: 200ko